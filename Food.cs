﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

namespace FoodOrderingSystem
{
    public class Food
    {
        public string code { get; }
        public string name { get; }
        public string foodCategory { get; }
        public float price { get; }
        public string image { get; }
        public string ingredients { get; }
        public float score { get; }
        public int vote { get; }

        private string CheckCode(string code)
        {
            string codePattern = "^([A-Za-z0-9]+)$";
            if (new Regex(codePattern).IsMatch(code))
                return code;
            throw new ArgumentException("Food code is invalid");
        }

        public Food(string code, string name, string foodCategory, float price, string image, string ingredients, float score, int vote)
        {
            this.code = CheckCode(code);
            this.name = name;
            this.foodCategory = foodCategory;
            this.price = price * 1.24f;
            this.image = image;
            this.ingredients = ingredients;
            this.score = score;
            this.vote = vote;
        }

        public FoodGrid ReadFoodGrid()
        {
            return new FoodGrid(this.code, this.name, this.foodCategory, this.price, this.ingredients, this.score, this.vote);
        }
    }

    public class FoodGrid
    {
        public string code { get; }
        public string name { get; }
        public string foodCategory { get; }
        public float price { get; }
        public string ingredients { get; }
        public float score { get; }
        public int vote { get; }
        public FoodGrid(string code, string name, string foodCategory, float price, string ingredients, float score, int vote)
        {
            this.code = code;
            this.name = name;
            this.foodCategory = foodCategory;
            this.price = price;
            this.ingredients = ingredients;
            this.score = score;
            this.vote = vote;
        }
        
    }

    public class FoodDataManager : ISelect<Food>
    {
        public Dictionary<string, string> foodCodeName { get; private set; }
        public Dictionary<string, float> foodCodeRawPrice { get; private set; }
        public List<Food> foodList = new List<Food>();
        public FoodDataManager()
        {
            foodCodeName = new Dictionary<string, string>();
            foodCodeRawPrice = new Dictionary<string, float>();
            foodList = Select();
        }

        // Select statement
        // this one will select all food
        public List<Food> Select()
        {
            string query = $"SELECT * FROM {TableName.Foods.ToString().ToLower()}";

            // Create a List to store the result
            List<Food> list = new List<Food>();
            foodCodeName = new Dictionary<string, string>();
            foodCodeRawPrice = new Dictionary<string, float>();

            DBConnect myRestaurantDatabase = App.db;

            //Open connection
            if (myRestaurantDatabase.OpenConnection() == true)
            {
                //Create Command
                MySqlCommand cmd = new MySqlCommand(query, myRestaurantDatabase.sqlConnection);
                //Create a data reader and Execute the command
                MySqlDataReader dataReader = cmd.ExecuteReader();

                //Read the data and store them in the list
                while (dataReader.Read())
                {
                    list.Add(
                        new Food
                        (
                            dataReader["id"] + "", 
                            dataReader["foodname"] + "", 
                            dataReader["category"] + "",
                            float.Parse(dataReader["price"] + ""),
                            dataReader["imagefile"] + "",
                            dataReader["ingredients"] + "",
                            float.Parse(dataReader["score"] + "" == "" ? "0" : dataReader["score"] + ""),
                            int.Parse(dataReader["vote"] + "")
                        )
                        );
                    foodCodeName.Add(list[list.Count - 1].code, list[list.Count - 1].name);
                    foodCodeRawPrice.Add(list[list.Count - 1].code, float.Parse(dataReader["price"] + ""));
                }

                //close Data Reader
                dataReader.Close();

                //close connection
                myRestaurantDatabase.CloseConnection();

                //return list to be displayed
                return list;
            }
            else
            {
                return list;
            }

        }

        // can throw exception
        public void AddFood(string code, string name, string foodCategory, float price, string image, string ingredients)
        {
            Dictionary<string, string> dict = new Dictionary<string, string>()
            {
                ["id"] = $"'{code}'",
                ["foodname"] = $"'{name}'",
                ["category"] = $"'{foodCategory}'",
                ["price"] = $"{price}",
                ["imagefile"] = $"'{image}'",
                ["ingredients"] = $"'{ingredients}'",
                ["score"] = $"{0}",
                ["vote"] = $"{0}"
            };

            App.db.Insert(dict, TableName.Foods.ToString().ToLower());
            // this can throw an exception if code/id is already registered.

            foodList = new List<Food>();
            foodList = Select();

        }
    }
}
