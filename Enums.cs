﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodOrderingSystem
{
    public enum UserType
    {
        Admin,
        Customer
    }

    public enum TableName
    {
        Orders,
        Customers,
        Admins,
        Foods,
        Calendar,
        FoodCategory,
        Carts,
        SpecialBonus,
        Drawing
    }

    public enum OrderType
    {
        All,
        A, // paid
        B // unpaid
    }

}
