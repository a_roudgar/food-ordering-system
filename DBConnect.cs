﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;
using System.Windows;
using System.CodeDom;
using Microsoft.Win32;
using MySqlX.XDevAPI.Relational;

namespace FoodOrderingSystem
{
    public class DBConnect
    {
        public MySqlConnection sqlConnection { get; private set; } // will be used to open a connection to the database.
        private string server; //indicates where our server is hosted, in our case, it's localhost.
        private string database; //is the name of the database we will use, 
                                 //in our case it's the database we already created earlier which is 'restaurant'.
        private string sqlUserID; //is our MySQL username.
        private string sqlPassword; //is our MySQL password.

        // constructor
        public DBConnect()
        {
            Initialize();
            // MessageBox.Show(OpenConnection().ToString());
            // MessageBox.Show(CloseConnection().ToString());
        }

        //Initialize values
        private void Initialize()
        {
            server = "localhost";
            database = "restaurant";
            sqlUserID = "root";
            sqlPassword = "20Amir79%";
            string connectionString;//contains the connection string to connect to the database,
                                    //and will be assigned to the connection variable.
            connectionString = "SERVER=" + server + ";" + "DATABASE=" +
                database + ";" + "UID=" + sqlUserID + ";" + "PASSWORD=" + sqlPassword + ";";
            sqlConnection = new MySqlConnection(connectionString);
            // MessageBox.Show(connectionString);
        }

        // open connection to database,
        // exeptions are handled inside the method,
        // returns true if connection is successful,
        // otherwise returns false (error messages are shown in the method)
        public bool OpenConnection()
        {
            try
            {
                 sqlConnection.Open();
                return true;
            }
            catch (MySqlException ex)
            {
                //0: Cannot connect to server
                //1045: Invalid user name and/or password
                switch (ex.Number)
                {
                    case 0:
                        MessageBox.Show("Cannot connect to server. Contact administrator");
                        break;
                    case 1045:
                        MessageBox.Show("Invalid username/password, please try again");
                        break;
                }
                return false;
            }
        }

        // Close connection,
        // exeptions are handled inside the method,
        // returns true if closure is successful,
        // otherwise returns false (error messages are shown in the method)
        public bool CloseConnection()
        {
            try
            {
                sqlConnection.Close();
                return true;
            }
            catch (MySqlException ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }

        // Insert statement
        // table name and a dictionary containing data will be passed, since we have multiple tables.
        public void Insert(Dictionary<string, string> newData, string tableName)
        {
            string[] tempKeys = newData.Select(x => x.Key).ToArray();
            string[] tempValues = newData.Select(x => x.Value).ToArray();
            string query = $"INSERT INTO {tableName} ({String.Join(", ", tempKeys)}) VALUES ({String.Join(", ", tempValues)});";

            try
            {
                //open connection
                if (this.OpenConnection() == true)
                {
                    //create command and assign the query and connection from the constructor
                    MySqlCommand cmd = new MySqlCommand(query, sqlConnection);

                    //Execute command
                    cmd.ExecuteNonQuery();

                    //close connection
                    this.CloseConnection();
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "InsertDataBaseError!");
                //close connection
                this.CloseConnection();
            }
            
        }

        // Update statement
        // table name, crieteria and updated fields will be passed, since we have multiple tables.
        public void Update(string criteriaString,string updatedFieldsInfo , string tableName)
        {            
            string query = $"SET SQL_SAFE_UPDATES = 0;UPDATE {tableName} SET {updatedFieldsInfo} WHERE {criteriaString};";

            try
            {
                //Open connection
                if (this.OpenConnection() == true)
                {
                    //create mysql command
                    MySqlCommand cmd = new MySqlCommand();
                    //Assign the query using CommandText
                    cmd.CommandText = query;
                    //Assign the connection using Connection
                    cmd.Connection = sqlConnection;

                    //Exexcute query
                    cmd.ExecuteNonQuery();

                    //close connsection
                    this.CloseConnection();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "UpdateDataBaseError!");
                //close connection
                this.CloseConnection();
            }

        }

        // Delete statement
        public void Delete(string criteriaString, string tableName)
        {
            string query = "SET SQL_SAFE_UPDATES = 0;DELETE FROM " + tableName + " WHERE " + criteriaString + ";";

            try
            {
                if (this.OpenConnection() == true)
                {
                    MySqlCommand cmd = new MySqlCommand(query, sqlConnection);
                    cmd.ExecuteNonQuery();
                    this.CloseConnection();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "DeleteDataBaseError!");
                //close connection
                this.CloseConnection();
            }
        }

        
        
    }
}