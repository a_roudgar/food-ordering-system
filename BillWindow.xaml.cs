﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FoodOrderingSystem
{
    /// <summary>
    /// Interaction logic for BillWindow.xaml
    /// </summary>
    public partial class BillWindow : Window
    {
        Customer customer;
        List<CartGrid> cartList;
        int finalBonus;
        string deliverDate;
        Image signature = new Image();
        public BillWindow(Customer customer, List<CartGrid> cartList, int finalBonus, string deliverDate)
        {
            InitializeComponent();
            this.customer = customer;
            this.cartList = cartList;
            this.finalBonus = finalBonus;
            this.deliverDate = deliverDate;
            this.signature.ReadSignature(customer.nationalID);
        }

        private void BuyButton_Click(object sender, RoutedEventArgs e)
        {
            PaymentWindow paymentWindow = new PaymentWindow(customer, cartList, finalBonus, deliverDate, signature);
            this.Close();
            paymentWindow.ShowDialog();      
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void BillList()
        {
            BillStackPanel.Children.Add(CreateTextBlock($"Customer : {customer.name} {customer.familyName}"));
            string billFormat = "{0, 10} |{1, -20} | {2, -15} | {3, 10} | {4}";
            string initial = string.Format(billFormat, "FoodCode", "FoodName", "PricePerUnit", "Amount", "FinalPrice");
            BillStackPanel.Children.Add(CreateTextBlock(initial));
            string listString = "";
            float totalPrice = 0f;
            foreach(CartGrid cartfood in cartList)
            {
                listString = string.Format
                    (billFormat, cartfood.code, cartfood.name, cartfood.pricePerUnit, cartfood.amount, cartfood.pricePerUnit * cartfood.amount);
                BillStackPanel.Children.Add(CreateTextBlock(listString));
                totalPrice += cartfood.pricePerUnit * cartfood.amount;
            }
            listString = $"rawPrice : {totalPrice}";
            BillStackPanel.Children.Add(CreateTextBlock(listString));
            totalPrice *= (100 - finalBonus) / 100.0f;
            totalPrice = float.Parse(totalPrice.ToString("0.00"));
            listString = $"discount : {finalBonus}%";
            BillStackPanel.Children.Add(CreateTextBlock(listString));
            listString = $"totalPrice : {totalPrice}";
            BillStackPanel.Children.Add(CreateTextBlock(listString));

        }

        private TextBlock CreateTextBlock(string text)
        {
            TextBlock textBlock = new TextBlock();
            textBlock.FontSize = 15;
            textBlock.FontFamily = new FontFamily("Verdana");
            textBlock.HorizontalAlignment = HorizontalAlignment.Left;
            textBlock.Text = text;
            return textBlock;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            BillList();
            BillStackPanel.Children.Add(signature);
        }
    }
}
