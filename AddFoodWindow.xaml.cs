﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FoodOrderingSystem
{
    /// <summary>
    /// Interaction logic for AddFoodWindow.xaml
    /// </summary>
    public partial class AddFoodWindow : Window
    {
        Category categories;
        List<string> categoryList;
        List<Dictionary<string, string>> categoryDict;
        public AddFoodWindow()
        {
            InitializeComponent();
            categories = new Category();
            categoryDict = categories.Select();
            categoryList = categoryDict.Select(x => x["category"]).ToList();
            this.CategoryComboBox.ItemsSource = categoryList;
        }

        private void ResetFoodButton_Click(object sender, RoutedEventArgs e)
        {
            this.CodeTextBox.Text = "";
            this.NameTextBox.Text = "";
            this.CategoryComboBox.Text = "";
            this.CategoryTextBox.Text = "";
            this.PriceTextBox.Text = "";
            this.IngredientsTextBox.Text = "";
        }

        private void CancelUpdateFoodButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void SaveFoodButton_Click(object sender, RoutedEventArgs e)
        {
            if (CodeTextBox.Text == "" || NameTextBox.Text == "" || (CategoryComboBox.Text == "" && CategoryTextBox.Text == "" ) 
                || PriceTextBox.Text == "" || PriceTextBox.Text == "" || IngredientsTextBox.Text == "")
            {
                MessageBox.Show("Every field must be filled. No Empty field should be left out.", "Registeration Error!");
            }
            else
            {
                
                try
                {
                    string code = CodeTextBox.Text;
                    string name = NameTextBox.Text;
                    string category = CategoryComboBox.Text + CategoryTextBox.Text;
                    
                    // capitalizing category string
                    string copy = category[0].ToString();
                    copy = copy.ToUpper();
                    for (int i = 1; i < category.Length; i++)
                        copy += category[i];
                    category = copy;

                    float price = int.Parse(PriceTextBox.Text);
                    string ingredients = IngredientsTextBox.Text;
                    string image = "";

                    if (!categoryList.Contains(category))
                    {
                        if (CategoryImage.Source == null)
                            throw new ArgumentException("You should choose a default image for the new category.");
                        image = CategoryImage.Source.ToString();
                        AddCategory(category, CategoryImage.Source.ToString());
                    }
                    else
                    {
                        image = categoryDict.Where(x => x["category"] == category).Select(x => x["defaultimage"]).First();
                    }

                    MessageBox.Show(category);
                    new FoodDataManager().AddFood(code, name, category, price, image, ingredients);
                    CodeTextBox.Text = code;
                    MessageBox.Show($"Food saved with code : {code}");
                    StreamWriter creator = new StreamWriter(App.opinionsFolder + code + ".txt");
                    if (creator != null)
                        creator.Close();
                }
                catch(Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error!");
                }
            }
        }

        private void AddCategory(string categoryName, string imagefile)
        {
            Dictionary<string, string> dict = new Dictionary<string, string>()
            {
                ["category"] = $"'{categoryName}'",
                ["defaultimage"] = $"'{imagefile}'"
            };
            App.db.Insert(dict, TableName.FoodCategory.ToString().ToString().ToLower());

            categories.Select();
        }

        private void CategoryComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.CategoryTextBox.Text = "";
            BrowseButton.IsEnabled = false;
        }

        private void CategoryTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            this.CategoryComboBox.Text = "";
            BrowseButton.IsEnabled = true;
        }

        private void BrowseButton_Click(object sender, RoutedEventArgs e)
        {
            CategoryImage.BrowseImage();
        }
    }
}
