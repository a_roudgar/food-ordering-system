﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using MySql.Data.MySqlClient;

namespace FoodOrderingSystem
{

    public abstract class User
    {
        public string name { get; }
        public string familyName { get; }
        public string email { get; }
        public string phoneNumber { get; }
        public string nationalID { get; }
        public string image { get; }

        protected User(string name, string familyName, string email, string phoneNumber, string nationalID, string image)
        {
            this.name = name;
            this.familyName = familyName;
            this.email = email;
            this.phoneNumber = phoneNumber;
            this.nationalID = nationalID;
            this.image = image;
        }

    }

    public class Customer : User
    {
        public string password { get; }
        public string address { get; }
        public Dictionary<string, int> codeVote { get; }
        public Customer
            (string name, string familyName, string email, string phoneNumber, string nationalID, 
            string address, string image, string password, Dictionary<string, int> codeVote) : 
            base(name, familyName, email, phoneNumber, nationalID, image)
        {
            this.password = password;
            this.address = address;
            this.codeVote = codeVote;
        }
    }

    public class Admin : User
    {
        public string userName { get; }
        public int loginCount { get; }
        public Admin(string name, string familyName, string email, string phoneNumber, string nationalID, string image, string userName, int loginCount):
            base(name, familyName, email, phoneNumber, nationalID, image)
        {
            this.userName = userName;
            this.loginCount = loginCount;
        }
    }

    public class CustomerDataManager : UsersManager, ISelect<Customer>
    {
        private string password;
        private const string passwordPattern =
            @"^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,16}$";
        private string Password
        {
            set
            {
                if (!CheckPatternValidity(value, passwordPattern))
                    throw new ArgumentException("Password is not strong Enough.\nYou should use at least one uppercase," +
                        " one lower case letter, one digit and one special character. " +
                        "Allowed length is between 8 and 16 characters.");
                password = value;
            }
        }

        List<Customer> customersList = new List<Customer>();

        //default constructor
        public CustomerDataManager()
        {
            customersList = Select();
        }

        public bool CanUpdate(string name, string familyName, string address, string email, string phoneNumber, string password)
        {
            // might throw an exception
            this.Name = name;
            this.FamilyName = familyName;
            this.Address = address;
            this.Email = email;
            this.PhoneNumber = phoneNumber;
            this.Password = password;

            return true;
        }

        // Select statement
        // this one will select all customers
        public List<Customer> Select()
        {
            string query = $"SELECT * FROM {TableName.Customers.ToString().ToLower()}";

            // Create a List to store the result
            List<Customer> list = new List<Customer>();

            DBConnect myRestaurantDatabase = App.db;

            //Open connection
            if (myRestaurantDatabase.OpenConnection() == true)
            {
                //Create Command
                MySqlCommand cmd = new MySqlCommand(query, myRestaurantDatabase.sqlConnection);
                //Create a data reader and Execute the command
                MySqlDataReader dataReader = cmd.ExecuteReader();

                //Read the data and store them in the list
                while (dataReader.Read())
                {
                    list.Add
                        (new Customer
                        (
                            dataReader["firstname"] + "",
                            dataReader["lastname"] + "",
                            dataReader["email"] + "",
                            dataReader["phonenumber"] + "",
                            dataReader["nationalid"] + "",
                            dataReader["address"] + "",
                            dataReader["imagefile"] + "",
                            dataReader["passcode"] + "",
                            this.RetrieveVotes(dataReader["votestring"] + "")
                        ));
                }

                //close Data Reader
                dataReader.Close();

                //close connection
                myRestaurantDatabase.CloseConnection();

                //return list to be displayed
                return list;
            }
            else
            {
                return list;
            }

        }

        // called in case of sign-up
        // it should be called when all the necessary fields are filled out and the register button is pressed
        public void SignUp(string name, string familyName, string address, string phoneNumber, string email, string nationalID, string password)
        {
            this.Name = name;
            this.FamilyName = familyName;            
            this.Email = email;
            this.PhoneNumber = phoneNumber;            
            this.NationalID = nationalID;
            this.Address = address;
            this.Password = password;
            // above assignments might throw an exception in case of wrong patterns,
            // otherwise we continue to save the data to sql.

            if (customersList.Where(x => x.nationalID == nationalID).Any())
                throw new ArgumentException("NationalID already registered!");

            // check if email is already registered
            // if yes, throw an exception
            if (customersList.Where(x => x.email == email).Any())
                throw new ArgumentException("Email already registered!");

            // check if phone number is already registered
            // if yes, throw an exception
            if (customersList.Where(x => x.phoneNumber == phoneNumber).Any())
                throw new ArgumentException("PhoneNumber already registered!");


            Dictionary<string, string> dict = new Dictionary<string, string>()
            {
                ["firstname"] = $"'{name}'",
                ["lastname"] = $"'{familyName}'",
                ["address"] = $"'{address}'",
                ["phonenumber"] = $"'{phoneNumber}'",
                ["email"] = $"'{email}'",
                ["nationalid"] = $"'{nationalID}'",
                ["passcode"] = $"'{password}'",
                ["votestring"] = "''"   
            };
           
            App.db.Insert(dict, TableName.Customers.ToString().ToLower());
            // this can throw an exception if nationalid is already registered.

            StreamWriter writer = null;
            try
            {
                writer = new StreamWriter(App.messagesFolder + nationalID + ".txt");
                writer.WriteLine("None");
            }
            catch
            {
                MessageBox.Show("An error occured while building your message system.", "MessageCreationError!");
            }
            finally
            {
                if (writer != null)
                    writer.Close();
            }
            customersList = new List<Customer>();
            customersList = Select();
        }

        // called in case of log-in
        // it should be called when the log-in key is pressed
        // returns a customer
        public Customer Login(string email, string password)
        {
            var search = customersList.Where(x => x.email == email);
            if (search.Any())
            {
                if (search.First().password == password)
                    return search.First();
                throw new ArgumentException("Password is incorrect!");
            }
            throw new ArgumentException("E-mail not registered!");
        }

        public Dictionary<string, int> RetrieveVotes(string voteString)
        {
            if (voteString == "" || voteString == null)
                return new Dictionary<string, int>();
            string[] eachCodeVote = voteString.Split(',');
            Dictionary<string, int> dict = new Dictionary<string, int>();
            foreach(string codeVote in eachCodeVote)
            {
                if(codeVote != "")
                {
                    string[] voteInfo = codeVote.Split('-');
                    dict.Add(voteInfo[0], int.Parse(voteInfo[1]));
                }              
            }
            return dict;
        }
    }

    public class AdminDataManager : UsersManager, ISelect<Admin>
    {
        List<Admin> adminsList = new List<Admin>();

        // private const string userNamePattern = @"^admin";

        public AdminDataManager()
        {
            adminsList = Select();
        }

        public bool CanUpdate(string email, string name, string familyName)
        {
            // might throw exception
            this.Email = email;
            this.Name = name;
            this.FamilyName = familyName;

            return true;
        }

        // Select statement
        // this one will select all admins
        public List<Admin> Select()
        {
            string query = $"SELECT * FROM {TableName.Admins.ToString().ToLower()}";

            // Create a List to store the result
            List<Admin> list = new List<Admin>();

            DBConnect myRestaurantDatabase = App.db;

            //Open connection
            if (myRestaurantDatabase.OpenConnection() == true)
            {
                //Create Command
                MySqlCommand cmd = new MySqlCommand(query, myRestaurantDatabase.sqlConnection);
                //Create a data reader and Execute the command
                MySqlDataReader dataReader = cmd.ExecuteReader();

                //Read the data and store them in the list
                while (dataReader.Read())
                {
                    list.Add
                        (new Admin
                        (
                            dataReader["firstname"] + "",
                            dataReader["lastname"] + "",
                            dataReader["email"] + "",
                            dataReader["phonenumber"] + "",
                            dataReader["nationalid"] + "",
                            dataReader["imagefile"] + "",
                            dataReader["username"] + "",
                            int.Parse(dataReader["logincounts"] + "")
                        ));
                }

                //close Data Reader
                dataReader.Close();

                //close connection
                myRestaurantDatabase.CloseConnection();

                //return list to be displayed
                return list;
            }
            else
            {
                return list;
            }

        }

        // called in case of log-in
        // it should be called when the log-in key is pressed
        // returns an Admin
        public Admin Login(string userName, string password)
        {
            var search = adminsList.Where(x => x.userName == userName);
            if (search.Any())
            {
                // the if statement itself can throw an exception
                if (CheckPassword(search.First().loginCount, search.First().userName) == password)
                {
                    Admin found = search.First();
                    App.db.Update($"username = '{found.userName}'", $"logincounts = {(found.loginCount + 1) % 10}", TableName.Admins.ToString().ToLower());
                    adminsList = new List<Admin>();
                    adminsList = Select();
                    return adminsList.Where(x => x.userName == userName).First();
                }
                    
                throw new ArgumentException("Password is incorrect!");
            }
            throw new ArgumentException("Username not registered!");
        }

        // check admin password on each login and on each usernamechange
        private string CheckPassword(int loginCount, string userName)
        {
            int mod10 = loginCount % 10;
            char[] vowels = new char[] { 'a', 'o', 'i', 'e', 'u', 'A', 'O', 'I', 'E', 'U' };
            int vowelCount = 0;
            foreach (char c in userName)
                vowelCount += vowels.Contains(c) ? 1 : 0;
            string password = "";
            for (int i = 0; i < mod10; i++)
                password += "1";
            for (int i = 0; i < vowelCount; i++)
                password += "0";

            return password;
        }
    }

    public abstract class UsersManager
    {

        protected const string emailPattern =
            @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
        protected const string fullNamePattern =
            @"^([A-Za-z]+)$";
        protected const string phoneNumberPattern =
            @"^(09|989|\+989|00989)[0-9]{9}$";

        // in case of sign-up and editing, this fields and their properties are used for data validation check
        protected string nationalID;
        protected string name;
        protected string familyName;
        protected string email;
        protected string phoneNumber;
        protected string address;

        public string FamilyName
        {
            get => familyName;
            protected set
            {
                if (!CheckPatternValidity(value, fullNamePattern))
                    throw new ArgumentException("FamilyName is invalid!");
                familyName = value;
            }
        }

        public string Name
        {
            get => name;
            protected set
            {
                if (!CheckPatternValidity(value, fullNamePattern))
                    throw new ArgumentException("Name is invalid!");
                name = value;
            }
        }

        public string PhoneNumber
        {
            get => phoneNumber;
            protected set
            {
                if (!CheckPatternValidity(value, phoneNumberPattern))
                    throw new ArgumentException("Phone Number pattern is incorrect!");
                phoneNumber = value;
            }
        }

        public string Email
        {
            get => email;
            protected set
            {
                if (!CheckPatternValidity(value, emailPattern))
                    throw new ArgumentException("E-mail Pattern is incorrect!");
                email = value;
            }
        }

        public string NationalID
        {
            get => nationalID;
            protected set
            {
                if (!value.CheckNationalID())
                    throw new ArgumentException("NationalID is invalid!");
                nationalID = value;
            }
        }

        

        public string Address
        {
            get => address;
            protected set
            {
                address = value;
            }
        }

        protected static bool CheckPatternValidity(string input, string pattern)
        {
            Regex regex = new Regex(pattern);
            if (regex.IsMatch(input))
                return true;
            return false;
        }
    }
}