﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FoodOrderingSystem
{
    /// <summary>
    /// Interaction logic for ChooseDateWindow.xaml
    /// </summary>
    public partial class ChooseDateWindow : Window
    {
        public DateTime date { get; private set; }
        public ChooseDateWindow()
        {
            InitializeComponent();
        }

        private void ApplyDateButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void OrderCalendar_SelectedDatesChanged(object sender, SelectionChangedEventArgs e)
        {
            date = (DateTime)OrderCalendar.SelectedDate;           
        }
    }
}
