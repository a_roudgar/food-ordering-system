﻿using System;
using System.Linq;
using System.Windows;

namespace FoodOrderingSystem
{
    /// <summary>
    /// Interaction logic for CustomerProfileWindow.xaml
    /// </summary>
    public partial class CustomerProfileWindow : Window
    {
        public Customer customer { get; private set; }
        CustomerDataManager CustomerDataManager;
        public CustomerProfileWindow(Customer customer)
        {
            InitializeComponent();
            this.customer = customer;
            CustomerDataManager = new CustomerDataManager();
        }

        private void BrowseImageButton_Click(object sender, RoutedEventArgs e)
        {
            ProfileImage.BrowseImage();
        }

        private void SaveInfoButton_Click(object sender, RoutedEventArgs e)
        {
            string tableName = TableName.Customers.ToString().ToLower();
            string criteriaString = $"nationalid='{customer.nationalID}'";
            string updatedFieldsInfo = "";

            try
            {
                CustomerDataManager testRepetitive = new CustomerDataManager();
                if(testRepetitive.Select().Where(x => x.email == EmailTextBox.Text).Any())
                {
                    if(testRepetitive.Select().Where(x => x.email == EmailTextBox.Text).First().nationalID != customer.nationalID)
                        throw new ArgumentException("This email is already being used by another customer.");
                }

                if(testRepetitive.Select().Where(x => x.phoneNumber == PhoneTextBox.Text).Any())
                {
                    if (testRepetitive.Select().Where(x => x.phoneNumber == PhoneTextBox.Text).First().nationalID != customer.nationalID)
                        throw new ArgumentException("This phone number is already being used by another customer.");
                }

                // might throw exceptions
                testRepetitive.CanUpdate
                    (FirstNameTextBox.Text, LastNameTextBox.Text, AddressTextBox.Text, EmailTextBox.Text, PhoneTextBox.Text, ProfilePasswordBox.Password);

                if(ProfilePasswordBox.Password != ConfirmPasswordBox.Password)
                {
                    ProfilePasswordBox.Password = "";
                    ConfirmPasswordBox.Password = "";
                    throw new Exception("Confirm your password Again!");
                }

                updatedFieldsInfo += $"firstname='{FirstNameTextBox.Text}', lastname='{LastNameTextBox.Text}', address='{AddressTextBox.Text}'," +
                    $" email='{EmailTextBox.Text}', phonenumber='{PhoneTextBox.Text}', passcode='{ProfilePasswordBox.Password}'";

                if(ProfileImage.Source != null)
                {
                    updatedFieldsInfo += $", imagefile='{ProfileImage.Source.ToString()}'";
                }
                else
                {
                    updatedFieldsInfo += ", imagefile=''";
                }

                App.db.Update(criteriaString, updatedFieldsInfo, tableName);
                customer = CustomerDataManager.Select().Where(x => x.nationalID == customer.nationalID).First();
                this.Close();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!");
            }
        }

        private void ResetButton_Click(object sender, RoutedEventArgs e)
        {
            Reset();
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Reset()
        {
            FirstNameTextBox.Text = customer.name;
            LastNameTextBox.Text = customer.familyName;
            AddressTextBox.Text = customer.address;
            EmailTextBox.Text = customer.email;
            PhoneTextBox.Text = customer.phoneNumber;
            NationalIDTextBox.Text = customer.nationalID;
            ProfilePasswordBox.Password = customer.password;
            ConfirmPasswordBox.Password = customer.password;
            ProfileImage.LoadImage(customer.image);
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Reset();
        }
    }
}
