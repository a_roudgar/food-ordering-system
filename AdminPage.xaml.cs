﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Controls.Primitives;

namespace FoodOrderingSystem
{
    /// <summary>
    /// Interaction logic for AdminPage.xaml
    /// </summary>
    public partial class AdminPage : Window
    {
        CalendarDataManager calendarDataManager;
        FoodDataManager foodDataManager;
        Admin admin;
        Category categories;
        List<Food> allFoodData;
        public AdminPage(Admin admin)
        {
            InitializeComponent();
            calendarDataManager = new CalendarDataManager();
            foodDataManager = new FoodDataManager();
            categories = new Category();
            allFoodData = foodDataManager.Select();
            this.admin = admin;

            DoDrawing();
        }

        private void DoDrawing()
        {
            StreamReader streamReader = null;
            string address = App.winnerInfoFile;
            try
            {
                streamReader = new StreamReader(address);
                string readLastDate = streamReader.ReadLine();
                if (streamReader != null)
                    streamReader.Close();
                DateTime? lastDateTime = null;
                lastDateTime = readLastDate.ToDateTime();
                if ((lastDateTime != null && DateTime.Now >= ((DateTime)lastDateTime).AddHours(24)) || lastDateTime == null)
                {
                    Drawings drawings = new Drawings();
                    List<string> winnerIDs = drawings.Roll();
                    if(winnerIDs.Count != 0)
                    {
                        StreamWriter writer = new StreamWriter(address);
                        writer.WriteLine(DateTime.Now);
                        foreach(string winnerID in winnerIDs)
                        {
                            writer.WriteLine(winnerID);
                        }
                        writer.Close();
                    }    
                }                    
                   
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "DrawingTimeError!");
            }
            finally
            {
                if (streamReader != null)
                    streamReader.Close();
            }
            
        }

        private void LoadFoodTable()
        {
            this.FoodDataGrid.ItemsSource = null;
            List<FoodGrid> foodGrids = new List<FoodGrid>();
            allFoodData = foodDataManager.Select();
            foreach (Food food in allFoodData)
                foodGrids.Add(food.ReadFoodGrid());
            this.FoodDataGrid.ItemsSource = foodGrids;
            this.FoodDataGrid.IsTextSearchCaseSensitive = false;
            this.FoodDataGrid.IsTextSearchEnabled = true;
        }

        private void LoadCalendarTable(string day)
        {           
            DayFoodDataGrid.ItemsSource = null;
            List<CalendarGrid> search = calendarDataManager.readDayData(day);
            foreach(CalendarGrid item in search)
            {
                if(search.Count == 0)
                {
                    calendarDataManager.DeleteFoodFromDay(item, day);
                }
            }
            DayFoodDataGrid.ItemsSource = search;    

            DayFoodDataGrid.IsTextSearchCaseSensitive = false;
            DayFoodDataGrid.IsTextSearchEnabled = true;

            if (search.Count < 3)
                AmountLimitTextBlock.Text = "This Day will not be shown to customers : less than 3 food kinds.";
            else
                AmountLimitTextBlock.Text = "";

            DateTime date = (DateTime)DayFoodDatePicker.SelectedDate;
            if (date < DateTime.Today)
                AmountLimitTextBlock.Text = "Days before today will not be shown to customers.";

        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            LoadFoodTable();
            DayFoodDatePicker.SelectedDate = DateTime.Today;
            DayFoodDatePicker.DisplayDateEnd = DateTime.Today.AddMonths(1);
            ShowDayMenu(DateTime.Today);            
        }

        private void AddFoodButton_Click(object sender, RoutedEventArgs e)
        {
            AddFoodWindow addFoodWindow = new AddFoodWindow();
            this.IsEnabled = false;
            addFoodWindow.ShowDialog();

            // refresh data
            this.IsEnabled = true;
            LoadFoodTable();
        }

        private void ResetFoodButton_Click(object sender, RoutedEventArgs e)
        {
            Food selectedFood = allFoodData.Where(x => x.code == ((FoodGrid)FoodDataGrid.SelectedItem).code).First();
            this.NameTextBox.Text = selectedFood.name;
            this.CategoryComboBox.Text = selectedFood.foodCategory;
            this.PriceTextBox.Text = selectedFood.price.ToString();
            this.IngredientsTextBox.Text = selectedFood.ingredients;
        }

        private void EditItemContextMenu_Click(object sender, RoutedEventArgs e)
        {
            EditFoodStackPanel.IsEnabled = true;
            Food selectedFood = allFoodData.Where(x => x.code == ((FoodGrid)FoodDataGrid.SelectedItem).code).First();
            FoodDataGrid.IsEnabled = false;

            this.CodeTextBox.Text = selectedFood.code;
            this.ScoreTextBox.Text = selectedFood.score.ToString();
            this.VoteTextBox.Text = selectedFood.vote.ToString();
            this.NameTextBox.Text = selectedFood.name;
            this.CategoryComboBox.ItemsSource = categories.Select().Select(x => x["category"]);
            this.CategoryComboBox.Text = selectedFood.foodCategory;
            this.PriceTextBox.Text = foodDataManager.foodCodeRawPrice[selectedFood.code].ToString();
            this.IngredientsTextBox.Text = selectedFood.ingredients;
            this.TotalTextBox.Text = TotalFoodCount(selectedFood).ToString();
        }

        // delete from foods table
        private void DeleteItemContextMenu_Click(object sender, RoutedEventArgs e)
        {
            Food selectedFood = allFoodData.Where(x => x.code == ((FoodGrid)FoodDataGrid.SelectedItem).code).First();
            App.db.Delete($"id='{selectedFood.code}'", TableName.Foods.ToString().ToLower());
            FoodDataGrid.ItemsSource = foodDataManager.Select();
            string address = App.opinionsFolder + selectedFood.code + ".txt";
            address.DeleteFile();
            List<string> dayLists = calendarDataManager.Select().Select(x => x.dayOfWeek).ToList();
            foreach(string date in dayLists)
            {
                List<CalendarGrid> dateData = calendarDataManager.readDayData(date);
                if(dateData.Where(x => x.code == selectedFood.code).Any())
                {
                    CalendarGrid currentDayFood = dateData.Where(x => x.code == selectedFood.code).First();
                    DeleteFoodFromDay(currentDayFood, date);
                }               
            }
        }

        private void EditRestaurantButton_Click(object sender, RoutedEventArgs e)
        {
            RestaurantInfoWindow restaurantInfoWindow = new RestaurantInfoWindow(ref this.admin);
            this.Close();
            restaurantInfoWindow.ShowDialog();
        }

        private void EditAdminInfoButton_Click(object sender, RoutedEventArgs e)
        {
            AdminPersonalInfo adminPersonalInfo = new AdminPersonalInfo(this.admin);
            this.IsEnabled = false;
            adminPersonalInfo.ShowDialog();

            this.IsEnabled = true;
            this.admin = adminPersonalInfo.admin;
        }

        private void FinancialButton_Click(object sender, RoutedEventArgs e)
        {
            FinancialWindow financialWindow = new FinancialWindow(admin);
            this.Close();
            financialWindow.Show();
        }

        private void EraseEditPanel()
        {
            this.CodeTextBox.Text = "";
            this.ScoreTextBox.Text = "";
            this.VoteTextBox.Text = "";
            this.NameTextBox.Text = "";
            this.CategoryComboBox.Text = "";
            this.PriceTextBox.Text = "";
            this.IngredientsTextBox.Text = "";
            this.TotalTextBox.Text = "";
            this.EditFoodStackPanel.IsEnabled = false;
        }

        private void CancelUpdateFoodButton_Click(object sender, RoutedEventArgs e)
        {
            EraseEditPanel();
            FoodDataGrid.IsEnabled = true;
        }

        private void EditFoodButton_Click(object sender, RoutedEventArgs e)
        {
            string criteriaString = "";
            criteriaString += $"id='{CodeTextBox.Text}'";
            string updatedFieldsInfo = "";
            updatedFieldsInfo += $"foodname='{NameTextBox.Text}'," + $"category='{CategoryComboBox.Text}'," 
                + $"price={float.Parse(PriceTextBox.Text)}," + $"ingredients='{IngredientsTextBox.Text}'";
            string tableName = TableName.Foods.ToString().ToLower();

            // save data
            App.db.Update(criteriaString, updatedFieldsInfo, tableName);
            // refresh food table data
            LoadFoodTable();
            // refresh calendar table data
            DateTime? date = DayFoodDatePicker.SelectedDate;
            if(date != null)
                ShowDayMenu((DateTime)date);

            EraseEditPanel();
            FoodDataGrid.IsEnabled = true;
        }

        // delete food from a specific day in calendar
        private void DeleteFromDay_Click(object sender, RoutedEventArgs e)
        {            
            CalendarGrid food = (CalendarGrid)DayFoodDataGrid.SelectedItem;
            DateTime today = DateTime.Today;
            string day = $"{today.Month}/{today.Day}/{today.Year}";
            DeleteFoodFromDay(food, day);
        }


        private int TotalFoodCount(Food food)
        {
            string[] dailyFoods = calendarDataManager.Select().Where(x => x.dayOfWeek.ToDateOnly() >= DateTime.Today).Select(x => x.dayFoodString).ToArray();
            int calendarCount = 0;
            for(int i = 0; i < dailyFoods.Length; i++)
            {
                string[] todayFoods = dailyFoods[i].Split(',');
                for(int j = 0; j < todayFoods.Length; j++)
                {
                    string[] eachFood = todayFoods[j].Split('-');
                    if(eachFood[0] == food.code)
                    {
                        calendarCount += int.Parse(eachFood[1]);
                    }
                }
            }
            return calendarCount;
        }

        private void RefreshFoodGridContextMenu_Click(object sender, RoutedEventArgs e)
        {
            LoadFoodTable();
        }

        private void DeleteFoodFromDay(CalendarGrid food, string day)
        {
            calendarDataManager.DeleteFoodFromDay(food, day);
            // refresh data
            LoadCalendarTable(day);

        }

        private void EditAmountForDay_Click(object sender, RoutedEventArgs e)
        {
            CalendarGrid food = (CalendarGrid)DayFoodDataGrid.SelectedItem;
            DateTime date = (DateTime)DayFoodDatePicker.SelectedDate;
            string day = $"{date.Month}/{date.Day}/{date.Year}";
            Food foodData = foodDataManager.Select().Where(x => x.code == food.code).First();
            FoodAmountWindow foodAmountWindow = new FoodAmountWindow(foodData, day);

            this.IsEnabled = false;
            foodAmountWindow.ShowDialog();
            this.IsEnabled = true;

            // refresh           
            ShowDayMenu(date);
        }

        // testing calendar system
        private void CalendarContextMenu_Click(object sender, RoutedEventArgs e)
        {
            Food selectedFood = allFoodData.Where(x => x.code == ((FoodGrid)FoodDataGrid.SelectedItem).code).First();
            AdminCalendar adminCalendar = new AdminCalendar(selectedFood);

            this.IsEnabled = false;
            adminCalendar.ShowDialog();
            this.IsEnabled = true;

            DateTime date = (DateTime)DayFoodDatePicker.SelectedDate;
            ShowDayMenu(date);
        }

        private void RefreshTodayMenu_Click(object sender, RoutedEventArgs e)
        {
            DateTime date = (DateTime)DayFoodDatePicker.SelectedDate;
            ShowDayMenu(date);
        }

        private void ShowDayMenu(DateTime date)
        {
            string dateString = $"{date.Month}/{date.Day}/{date.Year}";
            LoadCalendarTable(dateString);
        }

        private void DayFoodDatePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            DateTime date = (DateTime)DayFoodDatePicker.SelectedDate;
            ShowDayMenu(date);
        }

        private void SeeImageContextMenu_Click(object sender, RoutedEventArgs e)
        {
            Food selectedFood = allFoodData.Where(x => x.code == ((FoodGrid)FoodDataGrid.SelectedItem).code).First();
            PictureWindow pictureWindow = new PictureWindow(selectedFood.image, selectedFood.code, false);
            this.IsEnabled = false;
            pictureWindow.ShowDialog();
            this.IsEnabled = true;
        }

        private void EditImageContextMenu_Click(object sender, RoutedEventArgs e)
        {
            Food selectedFood = allFoodData.Where(x => x.code == ((FoodGrid)FoodDataGrid.SelectedItem).code).First();
            PictureWindow pictureWindow = new PictureWindow(selectedFood.image, selectedFood.code, true);
            this.IsEnabled = false;
            pictureWindow.ShowDialog();
            this.IsEnabled = true;
            // refresh
            LoadFoodTable();
            DateTime date = (DateTime)DayFoodDatePicker.SelectedDate;
            ShowDayMenu(date);
        }

        private void FoodDataGrid_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            DependencyObject dep = e.GetRightClickDependencyObject();
            dep.SetContextItemsEnabality(setFoodGridItemsEnablity);
        }

        private void setFoodGridItemsEnablity(bool b)
        {
            RefreshFoodGridContextMenu.IsEnabled = b;
            CalendarContextMenu.IsEnabled = b;
            EditItemContextMenu.IsEnabled = b;
            DeleteItemContextMenu.IsEnabled = b;
            SeeImageContextMenu.IsEnabled = b;
            EditImageContextMenu.IsEnabled = b;
        }

        private void DayFoodDataGrid_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            DependencyObject dep = e.GetRightClickDependencyObject();
            dep.SetContextItemsEnabality(SetDayGridItemsEnablity);
        }

        private void SetDayGridItemsEnablity(bool b)
        {
            DeleteFromDay.IsEnabled = b;
            EditAmountForDay.IsEnabled = b;
            RefreshTodayMenu.IsEnabled = b;
            DeleteDayMenu.IsEnabled = b;
            if(DayFoodDatePicker.SelectedDate < DateTime.Today && b == true)
            {
                DeleteFromDay.IsEnabled = false;
                EditAmountForDay.IsEnabled = false;
                RefreshTodayMenu.IsEnabled = true;
                DeleteDayMenu.IsEnabled = true;
            }
        }

        private void DeleteDayMenu_Click(object sender, RoutedEventArgs e)
        {
            DateTime currentDate = (DateTime)(DayFoodDatePicker.SelectedDate);
            string datestring = $"{currentDate.Month}/{currentDate.Day}/{currentDate.Year}";
            App.db.Delete($"datestring='{datestring}'", TableName.Calendar.ToString().ToLower());

            // delete carts with the same deliverdate
            App.db.Update($"deliverdate={datestring}", "registerdate='', foodstring=''", TableName.Carts.ToString().ToLower());

            // refresh
            ShowDayMenu(currentDate);
        }

        private void CommentsContextMenu_Click(object sender, RoutedEventArgs e)
        {
            string code = ((FoodGrid)FoodDataGrid.SelectedItem).code;
            CommentWindow commentWindow = new CommentWindow(code, true, null);
            this.IsEnabled = false;
            commentWindow.ShowDialog();
            this.IsEnabled = true;
        }
    }
}
