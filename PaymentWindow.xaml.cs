﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Text.RegularExpressions;
using System.IO;

namespace FoodOrderingSystem
{
    /// <summary>
    /// Interaction logic for PaymentWindow.xaml
    /// </summary>
    public partial class PaymentWindow : Window
    {
        Customer customer;
        List<CartGrid> cartList;
        int finalBonus;
        const string bankAccountPattern = @"^([0-9]){16}$";
        string deliverDate;
        Image signature;
        List<int> hours;
        List<int> minutues;
        public bool isBought { get; private set; } = false;
        public string orderID { get; private set; } = null;
        public PaymentWindow(Customer customer, List<CartGrid> cartList, int finalBonus, string deliverDate, Image signature)
        {
            InitializeComponent();
            this.customer = customer;
            this.cartList = cartList;
            this.finalBonus = finalBonus;
            this.deliverDate = deliverDate;
            this.signature = signature;
            this.signature.Height = 200;
            this.signature.Width = 350;
            hours = new List<int>();
            minutues = new List<int>();

            for (int i = 0; i < 24; i++)
                hours.Add(i);
            for (int i = 0; i < 60; i++)
                minutues.Add(i);       
        }

        private void OnlineCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            OfflineCheckBox.IsChecked = false;
            AccountTextBox.IsEnabled = true;
            AcceptButton.IsEnabled = true;
        }

        private void OfflineCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            OnlineCheckBox.IsChecked = false;
            AccountTextBox.Text = "";
            AccountTextBox.IsEnabled = false;
            AcceptButton.IsEnabled = true;
        }

        private void OnlineCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            AccountTextBox.Text = "";
            AccountTextBox.IsEnabled = false;
            AcceptButton.IsEnabled = false;
        }

        private void OfflineCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            AcceptButton.IsEnabled = false;
        }

        private void AcceptButton_Click(object sender, RoutedEventArgs e)
        {
            if(OfflineCheckBox.IsChecked == true)
            {
                string saved = SaveOrder("B");
                if (saved != null)
                {
                    MessageBox.Show($"Thanks for your shopping.\nOrder Code = {saved}", "Shopping Successful!");
                    isBought = true;
                    this.Close();
                }
                else
                    MessageBox.Show("Invalid time entered.", "InvalidDateTime!");
            }
            else
            {
                string saved = SaveOrder("A");
                if(saved != null)
                {
                    if (new Regex(bankAccountPattern).IsMatch(AccountTextBox.Text))
                    {
                        MessageBox.Show($"Thanks for your shopping.\nThe money has been withdrawed.\nOrder Code = {saved}", "Shopping Successful!");
                        isBought = true;
                        this.Close();
                    }
                    else
                    {
                        AccountTextBox.Text = "";
                        MessageBox.Show("Bank account number not valid.", "ShopTimeError!");
                    }
                }
                else
                {
                    MessageBox.Show("Invalid time entered.", "InvalidDateTime!");
                }
                
            }
                
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private string SaveOrder(string orderType)
        {
            OrdersDataManager ordersDataManager = new OrdersDataManager();
            CalendarDataManager calendarDataManager = new CalendarDataManager();
            FoodDataManager foodDataManager = new FoodDataManager();
            string foodString = "";
            DateTime deliverDateTime = 
                new DateTime(int.Parse(deliverDate.Split('/')[2]), int.Parse(deliverDate.Split('/')[0]), int.Parse(deliverDate.Split('/')[1]),
                int.Parse(HourComboBox.Text), int.Parse(MinuteComboBox.Text), 0);

            if (deliverDateTime <= DateTime.Now)
                return null;

            float totalPrice = 0f;
            foreach (CartGrid cartfooditem in cartList)
            {
                foodString += $"{cartfooditem.code}-{cartfooditem.amount}-{cartfooditem.pricePerUnit},";
                totalPrice += cartfooditem.pricePerUnit * cartfooditem.amount;
                int maxAmount = calendarDataManager.readDayData(deliverDate).Where(x => x.code == cartfooditem.code).Select(x => x.count).First();
                calendarDataManager.UpdateCalendar
                    (deliverDate, foodDataManager.Select().Where(x => x.code == cartfooditem.code).First(), maxAmount - cartfooditem.amount);
            }

            totalPrice *= (100 - finalBonus) / 100.0f;
            totalPrice = float.Parse(totalPrice.ToString("0.00"));

            // register to orders
            string ordercode = ordersDataManager.RegisterOrder(customer.nationalID, foodString, DateTime.Now, deliverDateTime.ToString(), orderType, finalBonus, totalPrice);
            signature.SaveImage(ordercode);
            this.orderID = ordercode;

            // delete from cart
            string tableName = TableName.Carts.ToString().ToLower();
            string updatedFieldsInfo = "registerdate='', deliverdate='', foodstring=''";
            string criteriaString = $"personid='{customer.nationalID}'";

            App.db.Update(criteriaString, updatedFieldsInfo, tableName);

            SpecialBonuses specialBonuses = new SpecialBonuses();
            int countOrders = ordersDataManager.Select().Where(x => x.personID == customer.nationalID).Count();
            if (countOrders % 7 == 0 || totalPrice >= 150)
            {
                specialBonuses.SaveToBonusList(customer.nationalID);
                if(countOrders > 12)
                {
                    Drawings drawings = new Drawings();
                    drawings.SaveToDrawings(customer.nationalID, ordercode);
                }
                    
            }
                
            else
                specialBonuses.DeleteFromBonusList(customer.nationalID);

            return ordercode;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            HourComboBox.ItemsSource = hours;
            MinuteComboBox.ItemsSource = minutues;
            HourComboBox.SelectedIndex = 12;
            MinuteComboBox.SelectedIndex = 0;
        }
    }
}
