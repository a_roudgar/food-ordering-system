﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FoodOrderingSystem
{
    /// <summary>
    /// Interaction logic for WinnerWindow.xaml
    /// </summary>
    public partial class WinnerWindow : Window
    {
        public WinnerWindow()
        {
            InitializeComponent();
        }

        private void AppetizerComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            AcceptButton.IsEnabled = true;
        }

        private void AcceptButton_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show($"Your appetizer ({AppetizerComboBox.Text}) will be delivered with your next order.");
            this.Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            FoodDataManager foodDataManager = new FoodDataManager();
            List<string> list = foodDataManager.Select().Where(x => x.foodCategory == "Appetizer").Select(x => x.name).ToList();
            AppetizerComboBox.ItemsSource = list;
        }
    }
}
