﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FoodOrderingSystem
{
    /// <summary>
    /// Interaction logic for FinancialWindow.xaml
    /// </summary>
    public partial class FinancialWindow : Window
    {
        List<OrderGrid> ordersList;
        List<Order> originalOrdersData;
        OrdersDataManager ordersDataManager;
        DateTime startDate;
        DateTime endDate;
        string orderType = "All";
        Admin admin;
        public FinancialWindow(Admin admin)
        {
            InitializeComponent();
            ordersDataManager = new OrdersDataManager();
            ordersList = new List<OrderGrid>();
            originalOrdersData = ordersDataManager.Select();
            List<string> personIDs = originalOrdersData.GroupBy(x => x.personID).Select(x => x.Key).ToList();
            foreach(string id in personIDs)
            {
                ordersList.AddRange(ordersDataManager.ReadOrderData(id));
            }
            startDate = DateTime.Today;
            endDate = DateTime.Today.AddDays(1);
            this.admin = admin;
        }

        private void StartDateButton_Click(object sender, RoutedEventArgs e)
        {
            startDate = this.ChooseDate();
        }

        private void OrderTypeComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBoxItem comboBoxItem = OrderTypeComboBox.SelectedItem as ComboBoxItem;
            orderType = comboBoxItem.Content.ToString();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            LoadFinancialTable(ordersList);
            CalculateExpenseSaleProfit(ordersList);
        }

        private void LoadFinancialTable(List<OrderGrid> tableList)
        {
            tableList = tableList.OrderByDescending(x => x.orderDateTime.ToDateTime()).ToList();
            FinancialDataGrid.ItemsSource = tableList;
            FinancialDataGrid.IsTextSearchCaseSensitive = false;
            FinancialDataGrid.IsTextSearchEnabled = true;
        }

        private void ApplyButton_Click(object sender, RoutedEventArgs e)
        {
            List<OrderGrid> tempList = new List<OrderGrid>();
            if(ordersList.Where(x => x.orderDateTime.CheckDateRange(startDate, endDate)).Any())
                tempList = ordersList.Where(x => x.orderDateTime.CheckDateRange(startDate, endDate)).ToList();

            if (orderType != "All")
                tempList = tempList.Where(x => x.orderType == orderType[0]).ToList();

            LoadFinancialTable(tempList);
            CalculateExpenseSaleProfit(tempList);
            
        }

        private void EndDateButton_Click(object sender, RoutedEventArgs e)
        {
            endDate = this.ChooseDate().AddDays(1);
        }

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            AdminPage adminPage = new AdminPage(admin);
            this.Close();
            adminPage.ShowDialog();
        }

        private void CalculateExpenseSaleProfit(List<OrderGrid> ordersList)
        {
            // calculate total sale & total expense
            float totalSale = 0f;
            float totalExpense = 0f;
            FoodDataManager foodDataManager = new FoodDataManager();
            foodDataManager.Select();
            foreach (OrderGrid orderItem in ordersList)
            {
                totalSale += orderItem.totalPrice;
                foreach(MainFoodData foodItem in orderItem.foodData)
                {
                    try
                    {
                        totalExpense += foodDataManager.foodCodeRawPrice[foodItem.code] * foodItem.amount;
                    }
                    // if the food is deleted and the code doesn't exist anymore
                    catch
                    {
                        totalExpense += foodItem.amount * 0.76f * foodItem.price;
                    }
                    
                }
            }
            float profit = totalSale - totalExpense;

            TotalSaleTextBlock.Text = $"Total Sale : {totalSale:0.00}";
            TotalExpenseTextBlock.Text = $"Total Expense : {totalExpense:0.00}";
            BenefitTextBlock.Text = $"Profit : {profit:0.00}";

        }

        private void CancelOrderContextItem_Click(object sender, RoutedEventArgs e)
        {
            OrderGrid selectedOrderGrid = (OrderGrid)FinancialDataGrid.SelectedItem;
            string id = originalOrdersData.Where(x => x.code == selectedOrderGrid.code).Select(x => x.personID).First();
            bool ifCanceled = selectedOrderGrid.CancelOrder(id);

            if (ifCanceled)
            {
                StreamReader reader = null;
                try
                {
                    reader = new StreamReader(App.messagesFolder + id + ".txt");
                    string input = reader.ReadLine();
                    if(input == "None")
                    {
                        if (reader != null)
                            reader.Close();
                        StreamWriter sw = new StreamWriter(App.messagesFolder + id + ".txt");
                        sw.Write("");
                        if (sw != null)
                            sw.Close();
                    }
                }
                catch
                {
                    MessageBox.Show($"Problem occured while writing messages to {selectedOrderGrid.fullName}.", "CancelOrderTimeError!");
                }
                finally
                {
                    if (reader != null)
                        reader.Close();
                }

                StreamWriter writer = null;
                try
                {
                    writer = new StreamWriter(App.messagesFolder + id + ".txt", true);
                    string format = "{0, -20} | {1, -10} | {2, -5}";
                    string initial = string.Format(format, "FoodName", "PricePerUnit", "Amount");
                    writer.WriteLine($"Your order with code {selectedOrderGrid.code} is canceled by the admin.");
                    writer.WriteLine(initial);
                    writer.WriteLine("**********************");
                    foreach (MainFoodData mainFoodData in selectedOrderGrid.foodData)
                    {
                        writer.WriteLine(format, mainFoodData.name, mainFoodData.price, mainFoodData.amount);
                    }
                    writer.WriteLine("**********************");
                    if (selectedOrderGrid.orderType == 'A')
                    {
                        writer.WriteLine("Payment Type : Online");
                        writer.WriteLine
                            ($"All the money will be paid back to your account : {selectedOrderGrid.totalPrice}\n");
                    }
                    else
                    {
                        writer.WriteLine("Payment Type : Offline\n");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "CancelOrderTimeError!");
                }
                finally
                {
                    if (writer != null)
                        writer.Close();
                }
            }

            // refresh
            ordersList = new List<OrderGrid>();
            originalOrdersData = ordersDataManager.Select();
            List<string> personIDs = originalOrdersData.GroupBy(x => x.personID).Select(x => x.Key).ToList();
            foreach (string personID in personIDs)
            {
                ordersList.AddRange(ordersDataManager.ReadOrderData(personID));
            }
            LoadFinancialTable(ordersList);
            CalculateExpenseSaleProfit(ordersList);
        }

        private void SignatureContextItem_Click(object sender, RoutedEventArgs e)
        {
            OrderGrid selectedOrderGrid = (OrderGrid)FinancialDataGrid.SelectedItem;
            SignatureCheckWindow signatureCheckWindow = new SignatureCheckWindow(selectedOrderGrid.code);
            this.IsEnabled = false;
            signatureCheckWindow.ShowDialog();
            this.IsEnabled = true;
        }

        private void FinancialDataGrid_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            DependencyObject dep = e.GetRightClickDependencyObject();
            dep.SetContextItemsEnabality(SetFinancialItemsEnablity);
        }

        private void SetFinancialItemsEnablity(bool b)
        {
            CancelOrderContextItem.IsEnabled = b;
            SignatureContextItem.IsEnabled = b;
        }
    }
}
