﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Markup;
using MySql.Data.MySqlClient;
using System.Windows;

namespace FoodOrderingSystem
{
    class DayFood
    {
        public string dayOfWeek { get; }
        public string dayFoodString { get; }

        public DayFood(string dayOfWeek, string dayFoodString)
        {
            this.dayOfWeek = dayOfWeek;
            this.dayFoodString = dayFoodString;
        }
    }

    // this class won't be able to have an Add() method for DayFoods,
    // because we want to prevent extra data (Max = 7 -> number of days in the week)
    // so we just use Update() from class DBConnect
    class CalendarDataManager : ISelect<DayFood>
    {
        public List<DayFood> calendarDataList = new List<DayFood>();

        public CalendarDataManager()
        {
            calendarDataList = Select();
        }

        // Select statement
        // this one will select all days with their food
        public List<DayFood> Select()
        {
            string query = $"SELECT * FROM {TableName.Calendar.ToString().ToLower()}";

            // Create a List to store the result
            List<DayFood> list = new List<DayFood>();

            DBConnect myRestaurantDatabase = App.db;

            //Open connection
            if (myRestaurantDatabase.OpenConnection() == true)
            {
                //Create Command
                MySqlCommand cmd = new MySqlCommand(query, myRestaurantDatabase.sqlConnection);
                //Create a data reader and Execute the command
                MySqlDataReader dataReader = cmd.ExecuteReader();

                //Read the data and store them in the list
                while (dataReader.Read())
                {
                    list.Add(
                        new DayFood
                        (
                            dataReader["datestring"] + "",
                            dataReader["dayfoodstring"] + ""
                        )
                        );
                }

                //close Data Reader
                dataReader.Close();

                //close connection
                myRestaurantDatabase.CloseConnection();

                //return list to be displayed
                return list;
            }
            else
            {
                return list;
            }
        }

        // change dayFoodString into readable data
        public List<CalendarGrid> readDayData(string day)
        {
            calendarDataList = this.Select();
            bool ifNotNull = calendarDataList.Where(x => x.dayOfWeek == day).Any();
            List<DayFood> search = new List<DayFood>();
            if (ifNotNull)
                search = calendarDataList.Where(x => x.dayOfWeek == day).Select(x => x).ToList();
            string rawInfo = null;
            if(search.Count != 0)
                rawInfo = search.Select(x => x.dayFoodString).First();

            string[] foodArray = rawInfo != null ? rawInfo.Split(',') : null;           
            FoodDataManager foodDataManager = new FoodDataManager();

            List<CalendarGrid> calendarGridList = new List<CalendarGrid>();
            if(foodArray != null)
            {
                for (int i = 0; i < foodArray.Length; i++)
                {
                    string[] codeFoodArray = foodArray[i].Split('-');

                    List<Food> tempFoodList = foodDataManager.Select();
                    bool b = tempFoodList.Where(x => x.code == codeFoodArray[0]).Any();
                    if (b)
                    {
                        Food f = tempFoodList.Where(x => x.code == codeFoodArray[0]).First();
                        calendarGridList.Add(new CalendarGrid(f.code, f.name, int.Parse(codeFoodArray[1])));
                    }

                }
            }
            

            return calendarGridList;
        }

        public void UpdateCalendar(string date, Food selectedFood, int amount)
        {
            string tableName = TableName.Calendar.ToString().ToLower();
            List<CalendarGrid> currentDayList = this.readDayData(date);

            string criteriaString = $"datestring='{date}'";
            string updatedFieldsInfo = "dayfoodString=";

            string updateString = "";
            foreach(CalendarGrid item in currentDayList)
            {
                if (item.code != selectedFood.code)
                    updateString += $"{item.code}-{item.count},";
            }
            updateString += $"{selectedFood.code}-{amount},";

            updatedFieldsInfo += $"'{updateString}'";

            App.db.Update(criteriaString, updatedFieldsInfo, tableName);
        }

        public void DeleteFoodFromDay(CalendarGrid food, string day)
        {
            // use update instead of delete, because food are not saved as a seperate record in this table.
            var dayInfo = this.readDayData(day);
            string[] newCodes = null;
            int[] newCount = null;

            newCodes = dayInfo.Select(x => x.code).ToArray();
            newCount = dayInfo.Select(x => x.count).ToArray();

            string[] tempUnary = null;
            tempUnary = new string[newCodes.Length];
            for (int i = 0; i < newCodes.Length; i++)
            {
                if(newCodes[i] != food.code)
                    tempUnary[i] = string.Join("-", newCodes[i], newCount[i]);
            }
            string updatedFieldsInfo = $"dayfoodstring='{string.Join(",", tempUnary)}'";
            string criteriaString = $"datestring='{day}'";
            string tableName = TableName.Calendar.ToString().ToLower();

            // delete using update statement
            App.db.Update(criteriaString, updatedFieldsInfo, tableName);
        }
    }

    class CalendarGrid
    {
        public string code { get; }
        public string name { get; }
        public int count { get; }
        public CalendarGrid(string code, string name, int count)
        {
            this.code = code;
            this.name = name;
            this.count = count;
        }
    }
}
