﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace FoodOrderingSystem
{
    public class Order
    {
        // must generate a unique code for each order
        // using order date & time and person id
        public string code { get; }
        public string personID { get; }
        public string foodString { get; }
        public string orderDateTime { get; }
        public string deliverDate { get; }
        public char orderType { get; }
        public int discount { get; }
        public float totalPrice { get; }

        public Order(string code, string personID, string foodString, string orderDateTime, string deliverDate,char orderType, int discount, float totalPrice)
        {
            this.code = code;
            this.personID = personID;
            this.foodString = foodString;
            this.orderDateTime = orderDateTime;
            this.deliverDate = deliverDate;
            this.orderType = orderType;
            this.discount = discount;
            this.totalPrice = totalPrice;
        }
    }

    public class OrdersDataManager : ISelect<Order>
    {
        public List<Order> ordersList = new List<Order>();
        public OrdersDataManager()
        {
            ordersList = Select();
        }
        
        // should not throw exception
        public string RegisterOrder(string personID, string foodString, DateTime orderDateTime, string deliverDate,string orderType, int discount,float totalPrice)
        {
            string code = GenerateCode(personID, orderDateTime);
            Dictionary<string, string> dict = new Dictionary<string, string>()
            {
                ["id"] = $"'{code}'",
                ["personid"] = $"'{personID}'",
                ["foodstring"] = $"'{foodString}'",
                ["orderdate"] = $"'{orderDateTime}'",
                ["deliverdate"] = $"'{deliverDate}'",
                ["ordertype"] = $"'{orderType}'",
                ["discount"] = $"{discount}",
                ["totalprice"] = $"{totalPrice}"
            };

            App.db.Insert(dict, TableName.Orders.ToString().ToLower());
            // this can throw an exception if code/id is already registered,
            // which should not happen because we used a method.

            ordersList = new List<Order>();
            ordersList = Select();

            return code;
        }

        private string GenerateCode(string personID, DateTime orderDateTime)
        {
            return $"{orderDateTime.Month}{orderDateTime.Day}{orderDateTime.Year}{orderDateTime.Hour}{orderDateTime.Minute}{orderDateTime.Second}{personID}";
        }

        // Select statement
        // this one will select all orders
        public List<Order> Select()
        {
            string query = $"SELECT * FROM {TableName.Orders.ToString().ToLower()}";

            // Create a List to store the result
            List<Order> list = new List<Order>();

            DBConnect myRestaurantDatabase = App.db;

            //Open connection
            if (myRestaurantDatabase.OpenConnection() == true)
            {
                //Create Command
                MySqlCommand cmd = new MySqlCommand(query, myRestaurantDatabase.sqlConnection);
                //Create a data reader and Execute the command
                MySqlDataReader dataReader = cmd.ExecuteReader();

                //Read the data and store them in the list
                while (dataReader.Read())
                {
                    list.Add(
                        new Order
                        (
                            dataReader["id"] + "",
                            dataReader["personid"] + "",
                            dataReader["foodstring"] + "",
                            dataReader["orderdate"] + "",
                            dataReader["deliverdate"] + "",
                            (dataReader["ordertype"] + "")[0],
                            int.Parse(dataReader["discount"] + ""),
                            float.Parse(dataReader["totalprice"] + "")
                        )
                        );
                }

                //close Data Reader
                dataReader.Close();

                //close connection
                myRestaurantDatabase.CloseConnection();

                //return list to be displayed
                return list;
            }
            else
            {
                return list;
            }
        }

        public List<OrderGrid> ReadOrderData(string personID)
        {
            CustomerDataManager customerDataManager = new CustomerDataManager();
            Customer thisCustomer = customerDataManager.Select().Where(x => x.nationalID == personID).First();
            List<Order> totalOrderData = this.Select();
            bool hasOrder = totalOrderData.Where(x => x.personID == personID).Any();
            List<Order> search = null;
            if (hasOrder)
                search = totalOrderData.Where(x => x.personID == personID).ToList();

            string[] rawInfo = new string[search != null ? search.Count : 0];

            FoodDataManager foodDataManager = new FoodDataManager();
            List<OrderGrid> orderGridList = new List<OrderGrid>();

            if (search != null)
            {
                for (int i = 0; i < search.Count; i++)
                {
                    rawInfo[i] = search[i].foodString;
                }

                for (int k = 0; k < search.Count; k++)
                {
                    string[] foodArray = rawInfo != null ? rawInfo[k].Split(',') : null;
                    if (foodArray != null)
                    {
                        List<MainFoodData> mainFoodDataList = new List<MainFoodData>();
                        for (int i = 0; i < foodArray.Length; i++)
                        {
                            string[] codeFoodArray = foodArray[i].Split('-');

                            List<Food> tempFoodList = foodDataManager.Select();
                            bool b = tempFoodList.Where(x => x.code == codeFoodArray[0]).Any();
                            if (b)
                            {
                                Food f = tempFoodList.Where(x => x.code == codeFoodArray[0]).First();
                                mainFoodDataList.Add(new MainFoodData(f.code, f.name, f.price, int.Parse(codeFoodArray[1])));

                                
                            }
                            else if(foodArray[i] != "")
                                mainFoodDataList.Add(new MainFoodData(codeFoodArray[0], "Deleted", float.Parse(codeFoodArray[2]), int.Parse(codeFoodArray[1])));    
                        }

                        orderGridList.Add
                            (new OrderGrid
                            (
                                search[k].code,
                                thisCustomer.name + " " + thisCustomer.familyName,
                                mainFoodDataList,
                                search[k].orderDateTime,
                                search[k].deliverDate,
                                search[k].orderType,
                                search[k].discount,
                                search[k].totalPrice
                            )
                            );
                    }
                }
            }
            // search.Count is equals to to rawinfo.Length             

            return orderGridList;
        }
    }

    public class OrderGrid
    {
        public string code { get; }
        public string fullName { get; }
        public List<MainFoodData> foodData { get; }
        public string orderDateTime { get; }
        public string deliverDate { get; }
        public char orderType { get; }
        public int discount { get; }
        public float totalPrice { get; }
        public OrderGrid
            (string code, string fullName, List<MainFoodData> foodData, string orderDateTime, string deliverDate, char orderType, int discount, float totalPrice)
        {
            this.code = code;
            this.fullName = fullName;
            this.foodData = foodData;
            this.orderDateTime = orderDateTime;
            this.deliverDate = deliverDate;
            this.orderType = orderType;
            this.discount = discount;
            this.totalPrice = totalPrice;
        }
    }

    public class MainFoodData
    {
        public string code { get; }
        public string name { get; }
        public float price { get; }
        public int amount { get; }
        public MainFoodData(string code, string name, float price, int amount)
        {
            this.code = code;
            this.name = name;
            this.price = price;
            this.amount = amount;
        }
    }
}
