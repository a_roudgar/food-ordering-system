﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.IO;
using System.Windows.Controls;

namespace FoodOrderingSystem
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public static DBConnect db = new DBConnect();
        public static string currentProjectPath { get; } = Directory.GetCurrentDirectory().ToString();
        public static string restaurantInfoFile { get; } = currentProjectPath + @"\Files\MyRestaurant.txt";
        public static string winnerInfoFile { get; } = currentProjectPath + @"\Files\Winner.txt";
        public static string messagesFolder { get; } = currentProjectPath + @"\Files\Messages\";
        public static string billsFolder { get; } = currentProjectPath + @"\Files\Bills\";
        public static string opinionsFolder { get; } = currentProjectPath + @"\Files\Opinions\";
        public static string menuImage { get; } = currentProjectPath + @"\Images\menu.png";
    }

   
}
