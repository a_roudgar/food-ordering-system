﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Microsoft.Win32;
using System.Text.RegularExpressions;

namespace FoodOrderingSystem
{
    /// <summary>
    /// Interaction logic for RestaurantInfoWindow.xaml
    /// </summary>
    public partial class RestaurantInfoWindow : Window
    {
        Admin admin;
        public RestaurantInfoWindow(ref Admin admin)
        {
            InitializeComponent();
            this.admin = admin;
        }

        private string[] ReadRestaurantInfo()
        {
            string fileAddress = App.restaurantInfoFile;
            string[] result = null;
            if (File.Exists(fileAddress))
            {
                StreamReader reader = null;
                try
                {
                    result = new string[5];
                    reader = new StreamReader(fileAddress);
                    int i = 0;
                    while (reader.EndOfStream == false)
                        result[i++] = reader.ReadLine();

                    for (int j = i; j < 5; j++)
                        result[j] = "";

                    return result;
                }
                catch
                {
                    MessageBox.Show("Could not read from file.", "File Error!");
                    return null;
                }
                finally
                {
                    if (reader != null)
                        reader.Close();
                }
                
            }
            else
            {
                MessageBox.Show("File not found!");
                return null;
            }
        }

        // show info in window
        private void SetInfo(string[] restaurantInfo)
        {
            RestaurantManagerTextBox.Text = admin.name + " " + admin.familyName;
            RestaurantRegionTextBox.Text = restaurantInfo[0];
            RestaurantTypeTextBox.Text = restaurantInfo[1];
            RestaurantAddressTextBox.Text = restaurantInfo[2];

            // loading image
            Uri fileUri = restaurantInfo[3] != "" ? new Uri(restaurantInfo[3]) : null;
            if (fileUri != null)
                MenuImage.Source = new BitmapImage(fileUri);
            else
                MenuImage.Source = null;

            PhoneTextBox.Text = restaurantInfo[4];
        }

        private void SaveRestaurantButton_Click(object sender, RoutedEventArgs e)
        {
            StreamWriter writer = null;
            try
            {
                string phoneNumberPattern = @"^(021)[0-9]{8}$";
                if (RestaurantRegionTextBox.Text == "" || RestaurantTypeTextBox.Text == "" || RestaurantAddressTextBox.Text == "" || PhoneTextBox.Text == "")
                    throw new ArgumentException("Every field must be filled.");
                if (MenuImage.Source == null)
                     throw new Exception("Image must be chosen.");
                if (new Regex(phoneNumberPattern).IsMatch(PhoneTextBox.Text) == false)
                    throw new ArgumentException("Phone Number Pattern is incorrect.");
                
                writer = new StreamWriter(App.restaurantInfoFile);
                writer.WriteLine(RestaurantRegionTextBox.Text);
                writer.WriteLine(RestaurantTypeTextBox.Text);
                writer.WriteLine(RestaurantAddressTextBox.Text);
                writer.WriteLine(MenuImage.Source.ToString());
                writer.WriteLine(PhoneTextBox.Text);
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!");
            }
            finally
            {
                if (writer != null)
                    writer.Close();
            }
        }

        // reset data in window to original
        private void ResetRestaurantButton_Click(object sender, RoutedEventArgs e)
        {
            string[] readData = ReadRestaurantInfo();
            if (readData != null)
                SetInfo(readData);
        }

        private void BackToAdminPageButton_Click(object sender, RoutedEventArgs e)
        {
            AdminPage adminPage = new AdminPage(admin);
            this.Close();
            adminPage.ShowDialog();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            
            string[] readData = ReadRestaurantInfo();
            if (readData != null)
                SetInfo(readData);
        }

        private void BrowseButton_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Multiselect = false; 
            // i will filter it later
            openFileDialog.InitialDirectory = App.currentProjectPath;
            if (openFileDialog.ShowDialog() == true)
            {
                Uri fileUri = new Uri(openFileDialog.FileName);
                MenuImage.Source = new BitmapImage(fileUri);
            }
        }
    }
}
