﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace FoodOrderingSystem
{
    class Category : ISelect<Dictionary<string, string>>
    {
        public List<Dictionary<string, string>> categories { get; private set; }

        public Category()
        {
            categories = Select();
        }

        // Select statement
        // this one will select all categories
        public List<Dictionary<string, string>> Select()
        {
            string query = $"SELECT * FROM {TableName.FoodCategory.ToString().ToLower()}";

            // Create a List to store the result
            List<Dictionary<string, string>> list = new List<Dictionary<string, string>>();

            DBConnect myRestaurantDatabase = App.db;

            //Open connection
            if (myRestaurantDatabase.OpenConnection() == true)
            {
                //Create Command
                MySqlCommand cmd = new MySqlCommand(query, myRestaurantDatabase.sqlConnection);
                //Create a data reader and Execute the command
                MySqlDataReader dataReader = cmd.ExecuteReader();

                //Read the data and store them in the list
                while (dataReader.Read())
                {
                    list.Add(
                        new Dictionary<string, string>() 
                        { ["category"] = dataReader["category"] + "", ["defaultimage"] = dataReader["defaultimage"] + "" }
                        );
                }

                //close Data Reader
                dataReader.Close();

                //close connection
                myRestaurantDatabase.CloseConnection();

                //return list to be displayed
                return list;
            }
            else
            {
                return list;
            }
        }

        public Dictionary<string, string> this[string categoryName]
        {
            get
            {
                return categories.Where(x => x["category"] == categoryName).First();
            }
        }
    }
}
