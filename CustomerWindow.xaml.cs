﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace FoodOrderingSystem
{
    /// <summary>
    /// Interaction logic for CustomerWindow.xaml
    /// </summary>
    public partial class CustomerWindow : Window
    {
        FoodDataManager foodDataManager;
        CalendarDataManager calendarDataManager;
        List<Food> menuDataList;
        List<CalendarGrid> dayMenu;
        DateTime searchDate;
        Customer customer;
        bool isfullMenuOn = false;

        Category categories;
        string selectedCategory = "All";
        string ingredientSearchText = "";
        string nameSearch = "";
        int? maxPrice = null;
        public CustomerWindow(Customer customer)
        {
            InitializeComponent();
            foodDataManager = new FoodDataManager();
            calendarDataManager = new CalendarDataManager();
            searchDate = DateTime.Today;
            string searchDateString = $"{searchDate.Month}/{searchDate.Day}/{searchDate.Year}";
            menuDataList = foodDataManager.Select();
            dayMenu = calendarDataManager.readDayData(searchDateString);
            this.customer = customer;
            this.categories = new Category();

            IfWinner();
            ReadMessages();
        }

        private void IfWinner()
        {
            string drawingAddress = App.winnerInfoFile;
            StreamReader reader = null;
            StreamWriter writer = null;
            try
            {
                reader = new StreamReader(drawingAddress);
                string date = reader.ReadLine();
                string otherWinners = "";
                bool hasWon = false;
                if(date != "default")
                {
                    while (reader.EndOfStream == false)
                    {
                        string input = reader.ReadLine();
                        if (input == customer.nationalID)
                        {
                            hasWon = true;
                        }
                        else
                        {
                            otherWinners += input + "\n";
                        }
                    }

                    reader.Close();
                    if (hasWon)
                    {
                        MessageBox.Show("You won the drawing", "Congratulations!");
                        SelectAppetizer();
                    }
                        

                    writer = new StreamWriter(drawingAddress);
                    writer.WriteLine(date);
                    writer.Write(otherWinners);
                    writer.Close();
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "ReadWinnerTimeError!");
            }
            finally
            {
                if (reader != null)
                    reader.Close();
                if (writer != null)
                    writer.Close();
            }
        }

        private void SelectAppetizer()
        {
            new WinnerWindow().ShowDialog();
        }

        private void ReadMessages()
        {
            StreamReader reader = null;
            StreamWriter writer = null;
            try
            {
                reader = new StreamReader(App.messagesFolder + customer.nationalID + ".txt");
                string input = "";
                input = reader.ReadLine();
                if(input != "None")
                {
                    while(reader.EndOfStream == false)
                    {
                        input += reader.ReadLine();
                    }

                    MessageBox.Show(input, "Message");
                }

                reader.Close();

                writer = new StreamWriter(App.messagesFolder + customer.nationalID + ".txt");
                writer.WriteLine("None");
                writer.Close();
            }
            catch
            {
                MessageBox.Show("An error occured while reading your messages", "ReadMessageTimeError!");
            }
            finally
            {
                if (reader != null)
                    reader.Close();
                if (writer != null)
                    writer.Close();
            }
        }

        private void CustomerFoodCalendar_SelectedDatesChanged(object sender, SelectionChangedEventArgs e)
        {
            searchDate = (DateTime)CustomerFoodCalendar.SelectedDate;
            string searchDateString = $"{searchDate.Month}/{searchDate.Day}/{searchDate.Year}";
            dayMenu = calendarDataManager.readDayData(searchDateString);
            LoadMenu();
        }

        private void CartButton_Click(object sender, RoutedEventArgs e)
        {
            CartWindow cartWindow = new CartWindow(customer);
            this.Close();
            cartWindow.ShowDialog();
        }

        private void ProfileButton_Click(object sender, RoutedEventArgs e)
        {
            CustomerProfileWindow customerProfileWindow = new CustomerProfileWindow(customer);
            this.IsEnabled = false;
            customerProfileWindow.ShowDialog();
            this.IsEnabled = true;
            this.customer = customerProfileWindow.customer;
        }

        private void OrdersButton_Click(object sender, RoutedEventArgs e)
        {
            CustomerOrderWindow customerOrderWindow = new CustomerOrderWindow(customer);
            this.Close();
            customerOrderWindow.ShowDialog();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            LoadMenu();
            List<string> items = new List<string>();
            items.Add(selectedCategory);
            items.AddRange(categories.Select().Select(x => x["category"]));
            CategoryChoiceComboBox.ItemsSource = items;
            CategoryChoiceComboBox.Text = selectedCategory;
            //NameSearchComboBox.ItemsSource = menuDataList.Select(x => x.name).ToList();

            this.CustomerFoodCalendar.DisplayDateStart = DateTime.Today;
            this.CustomerFoodCalendar.DisplayDateEnd = DateTime.Today.AddMonths(1);
        }

        private void LoadMenu()
        {
            List<FoodGrid> menuList = new List<FoodGrid>();
            foreach(var dayItem in dayMenu)
            {
                foreach(var menuItem in menuDataList)
                {
                    if (dayItem.code == menuItem.code)
                    {
                        if(dayItem.count > 0)
                            menuList.Add
                                (new FoodGrid
                                (menuItem.code, menuItem.name, menuItem.foodCategory, menuItem.price, menuItem.ingredients, menuItem.score, menuItem.vote)
                                );
                        else
                        {
                            string searchDateString = $"{searchDate.Month}/{searchDate.Day}/{searchDate.Year}";
                            calendarDataManager.DeleteFoodFromDay(dayItem, searchDateString);
                        }
                    }
                        
                }
            }

            if (menuList.Count >= 3)
                this.MenuDataGrid.ItemsSource = menuList;
            else
            {
                this.MenuDataGrid.ItemsSource = new List<FoodGrid>();
                MessageBox.Show("We have no foods registered for this date or we ran out of foods for this date.", "Sorry!");
            }    
                
            this.MenuDataGrid.IsTextSearchCaseSensitive = false;
            this.MenuDataGrid.IsTextSearchEnabled = true;            
        }

        private void LoadFullMenu()
        {
            List<FoodGrid> menuList = new List<FoodGrid>();
            foreach (var menuItem in menuDataList)
            {
                 menuList.Add
                     (new FoodGrid
                     (menuItem.code, menuItem.name, menuItem.foodCategory, menuItem.price, menuItem.ingredients, menuItem.score, menuItem.vote)
                     );
            }
            this.MenuDataGrid.ItemsSource = menuList;
            this.MenuDataGrid.IsTextSearchCaseSensitive = false;
            this.MenuDataGrid.IsTextSearchEnabled = true;
        }

        private void ApplyFilterButton_Click(object sender, RoutedEventArgs e)
        {
            // registering filter search changes
            selectedCategory = CategoryChoiceComboBox.Text;
            ingredientSearchText = IngredientSearchTextBox.Text.Trim().ToLower();
            nameSearch = NameSearchTextBox.Text.Trim().ToLower();
            //NameSearchComboBox.ItemsSource = menuDataList.Select(x => x.name).ToList();
            //NameSearchComboBox.Text = "";

            try
            {
                maxPrice = int.Parse(MaxPriceTextBox.Text.Trim());
            }
            catch
            {
                maxPrice = null;
            }

            // applying filter search changes
            menuDataList = foodDataManager.Select();
            if(ingredientSearchText != "" || ingredientSearchText != null)
                menuDataList = menuDataList.SearchMenu(x => x.ingredients.ToLower().Contains(ingredientSearchText.ToLower()));
            if(nameSearch != "" || nameSearch != null)
                menuDataList = menuDataList.SearchMenu(x => x.name.ToLower().Contains(nameSearch.ToLower()));
            if(selectedCategory != "All")
                menuDataList = menuDataList.SearchMenu(x => x.foodCategory == selectedCategory);
            if (maxPrice != null)
                menuDataList = menuDataList.SearchMenu(x => x.price <= (float)maxPrice);

            // refresh the search
            if (isfullMenuOn)
                LoadFullMenu();
            else
                LoadMenu();

        }

        private void ResetFilterButton_Click(object sender, RoutedEventArgs e)
        {
            menuDataList = foodDataManager.Select();
            IngredientSearchTextBox.Text = "";
            NameSearchTextBox.Text = "";
            MaxPriceTextBox.Text = "";
            //NameSearchComboBox.ItemsSource = menuDataList.Select(x => x.name).ToList();
            //NameSearchComboBox.Text = "";
            // refresh the menu
            if (isfullMenuOn)
                LoadFullMenu();
            else
                LoadMenu();
            CategoryChoiceComboBox.Text = "All";
        }

        private void AddToCartContextItem_Click(object sender, RoutedEventArgs e)
        {
            Food selectedFood = menuDataList.Where(x => x.code == ((FoodGrid)MenuDataGrid.SelectedItem).code).First();
            string deliverDate = $"{searchDate.Month}/{searchDate.Day}/{searchDate.Year}";
            FoodCartAmountWindow foodCartAmountWindow = new FoodCartAmountWindow(selectedFood, customer.nationalID, deliverDate);
            this.IsEnabled = false;
            foodCartAmountWindow.ShowDialog();
            this.IsEnabled = true;  
        }

        private void SeeImageContextMenu_Click(object sender, RoutedEventArgs e)
        {
            Food selectedFood = menuDataList.Where(x => x.code == ((FoodGrid)MenuDataGrid.SelectedItem).code).First();
            PictureWindow pictureWindow = new PictureWindow(selectedFood.image, selectedFood.code, false);
            this.IsEnabled = false;
            pictureWindow.ShowDialog();
            this.IsEnabled = true;
        }

        private void RateContextItem_Click(object sender, RoutedEventArgs e)
        {
            Food selectedFood = menuDataList.Where(x => x.code == ((FoodGrid)MenuDataGrid.SelectedItem).code).First();
            RateWindow rateWindow = new RateWindow(customer, selectedFood);
            this.IsEnabled = false;
            rateWindow.ShowDialog();
            this.IsEnabled = true;
            customer = rateWindow.customer;

            menuDataList = foodDataManager.Select();
            // refresh the menu
            if (isfullMenuOn)
                LoadFullMenu();
            else
                LoadMenu();
            CategoryChoiceComboBox.Text = "All";
        }

        private void FullMenuButton_Click(object sender, RoutedEventArgs e)
        {
            isfullMenuOn = !isfullMenuOn;
            if (isfullMenuOn)
                FullMenuOn();
            else
                FullMenuOff();
        }

        private void FullMenuOn()
        {
            CustomerFoodCalendar.IsEnabled = false;
            CartButton.IsEnabled = false;
            AddToCartContextItem.IsEnabled = false;
            FullMenuButton.Content = "See Calendar Menu";
            LoadFullMenu();            
        }

        private void FullMenuOff()
        {
            CustomerFoodCalendar.IsEnabled = true;
            CartButton.IsEnabled = true;
            AddToCartContextItem.IsEnabled = true;
            CustomerFoodCalendar.SelectedDate = searchDate;
            FullMenuButton.Content = "See Full Menu";
            LoadMenu();            
        }

        private void MenuImageContextMenu_Click(object sender, RoutedEventArgs e)
        {
            PictureWindow pictureWindow = new PictureWindow(App.menuImage, null, false);
            this.IsEnabled = false;
            pictureWindow.Height = 800;
            pictureWindow.Width = 1000;
            pictureWindow.ShowDialog();
            this.IsEnabled = true;
        }

        private void ReadOpinionsContextMenu_Click(object sender, RoutedEventArgs e)
        {
            string code = ((FoodGrid)MenuDataGrid.SelectedItem).code;
            CommentWindow commentWindow = new CommentWindow(code, true, customer);
            this.IsEnabled = false;
            commentWindow.ShowDialog();
            this.IsEnabled = true;
        }

        private void AddOpinionContextMenu_Click(object sender, RoutedEventArgs e)
        {
            string code = ((FoodGrid)MenuDataGrid.SelectedItem).code;
            CommentWindow commentWindow = new CommentWindow(code, false, customer);
            this.IsEnabled = false;
            commentWindow.ShowDialog();
            this.IsEnabled = true;
        }

        private void MenuDataGrid_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            DependencyObject dep = e.GetRightClickDependencyObject();
            dep.SetContextItemsEnabality(SetItemsEnablity);
        }

        private void SetItemsEnablity(bool b)
        {
            AddToCartContextItem.IsEnabled = b;
            if (isfullMenuOn)
                AddToCartContextItem.IsEnabled = false;
            SeeImageContextMenu.IsEnabled = b;
            RateContextItem.IsEnabled = b;
            MenuImageContextMenu.IsEnabled = b;
            ReadOpinionsContextMenu.IsEnabled = b;
            AddOpinionContextMenu.IsEnabled = b;
        }
    }
}
