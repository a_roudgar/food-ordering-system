﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Text.RegularExpressions;

namespace FoodOrderingSystem
{
    /// <summary>
    /// Interaction logic for CartWindow.xaml
    /// </summary>
    public partial class CartWindow : Window
    {
        Customer customer;        
        CartFoods foodCartData;
        CartDataManager cartDataManager;
        FoodDataManager foodDataManager;
        CalendarDataManager calendarDataManager;
        string deliverDate;
        string registerDate;
        List<CartGrid> cartMenuList;
        int generalBonus = 0;
        int specialBonus = 0;
        public CartWindow(Customer customer)
        {
            InitializeComponent();
            this.customer = customer;
            cartDataManager = new CartDataManager();
            foodDataManager = new FoodDataManager();
            calendarDataManager = new CalendarDataManager();
            foodCartData = cartDataManager.ReadCartData(customer.nationalID);
            
            registerDate = cartDataManager.FindPersonCart(customer.nationalID).registerDate;
            string todayDate = $"{DateTime.Today.Month}/{DateTime.Today.Day}/{DateTime.Today.Year}";
            if(registerDate != todayDate)
            {
                string tableName = TableName.Carts.ToString().ToLower();
                string updatedFieldsInfo = $"registerdate='{registerDate}', deliverdate='{deliverDate}', foodstring=''";
                string criteriaString = $"personid='{customer.nationalID}'";

                App.db.Update(criteriaString, updatedFieldsInfo, tableName);
                foodCartData = cartDataManager.ReadCartData(customer.nationalID);
            }

            deliverDate = cartDataManager.FindPersonCart(customer.nationalID).deliverDate;
        }

        private void RemoveContextItem_Click(object sender, RoutedEventArgs e)
        {
            CartGrid selectedFood = (CartGrid)CartDataGrid.SelectedItem;
            RemoveFromCart(selectedFood.code);  
        }

        private void AmountContextItem_Click(object sender, RoutedEventArgs e)
        {
            CartGrid selectedFood = (CartGrid)CartDataGrid.SelectedItem;
            Food findFood = foodDataManager.Select().Where(x => x.code == selectedFood.code).First();
            FoodCartAmountWindow foodCartAmountWindow = new FoodCartAmountWindow(findFood, customer.nationalID, deliverDate);
            this.IsEnabled = false;
            foodCartAmountWindow.ShowDialog();
            this.IsEnabled = true;
            //refresh
            foodCartData = cartDataManager.ReadCartData(customer.nationalID);
            LoadCart();
        }

        private void BuyButton_Click(object sender, RoutedEventArgs e)
        {
            List<CalendarGrid> calendarGrids = calendarDataManager.readDayData(deliverDate);
            // check for bonuses
            CheckForGeneralBonus();
            CheckForSpecialBonus();
            try
            {
                foreach (KeyValuePair<Food, int> foodamount in foodCartData.cartFoodData)
                {
                    if (calendarGrids.Where(x => x.code == foodamount.Key.code).Any())
                    {
                        CalendarGrid thisFood = calendarGrids.Where(x => x.code == foodamount.Key.code).First();
                        if (thisFood.count < foodamount.Value)
                            throw new ArgumentException($"Not enough amount left for {thisFood.name}.\nAvailable amount = {thisFood.count}");      
                    }
                    else
                    {
                        throw new Exception($"{foodamount.Key.name} is no more available.");
                    }

                    // ask for signature and bank account
                    SignatureWindow signatureWindow = new SignatureWindow(customer, cartMenuList, generalBonus + specialBonus, deliverDate);
                    this.IsEnabled = false;
                    signatureWindow.ShowDialog();
                    this.IsEnabled = true;
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "BuyTimeError!");
            }
            finally
            {
                deliverDate = cartDataManager.FindPersonCart(customer.nationalID).deliverDate;
                DateTextBlock.Text = "Today Requests for deliveration date: " + deliverDate;
                //refresh
                foodCartData = cartDataManager.ReadCartData(customer.nationalID);
                LoadCart();
            }
            
        }

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            CustomerWindow customerWindow = new CustomerWindow(customer);
            this.Close();
            customerWindow.ShowDialog();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            LoadCart();
            DateTextBlock.Text += deliverDate;
        }

        private void LoadCart()
        {
            if (foodCartData != null)
            {
                BuyButton.IsEnabled = true;
                if (foodCartData.cartFoodData != null)
                {
                    List<CartGrid> list = new List<CartGrid>();
                    foreach (KeyValuePair<Food, int> item in foodCartData.cartFoodData)
                    {
                        list.Add(new CartGrid(item.Key.code, item.Key.name, item.Value, item.Key.price));
                    }
                    CartDataGrid.ItemsSource = list;
                    cartMenuList = list;
                }

                else
                {
                    DateTextBlock.Text = "Today Requests for deliveration date: ";
                    BuyButton.IsEnabled = false;
                    cartMenuList = null;
                    CartDataGrid.ItemsSource = null;
                    MessageBox.Show("Cart is currently Empty");
                }
            }
            else
            {
                DateTextBlock.Text = "Today Requests for deliveration date: ";
                BuyButton.IsEnabled = false;
                cartMenuList = null;
                CartDataGrid.ItemsSource = null;
                MessageBox.Show("Cart is currently Empty");
            }
                
        }

        private void RemoveFromCart(string foodCode)
        {
            string criteriastring = $"personid='{customer.nationalID}'";
            string tableName = TableName.Carts.ToString().ToLower();
            string updatedFieldsInfo = "foodstring='";

            foreach(KeyValuePair<Food, int> item in foodCartData.cartFoodData)
            {
                if(item.Key.code != foodCode)
                    updatedFieldsInfo += $"{item.Key.code}-{item.Value},";
            }
            updatedFieldsInfo += "', registerdate='', deliverdate=''";

            App.db.Update(criteriastring, updatedFieldsInfo, tableName);
            //refresh
            foodCartData = cartDataManager.ReadCartData(customer.nationalID);
            if(foodCartData == null)
            {
                BuyButton.IsEnabled = false;
                DateTextBlock.Text = "Today Requests for deliveration date: ";
            }
            else if (foodCartData.cartFoodData.Count == 0)
            {
                BuyButton.IsEnabled = false;
                DateTextBlock.Text = "Today Requests for deliveration date: ";
            }
            LoadCart();
        }

        private void CheckForGeneralBonus()
        {
            OrdersDataManager ordersDataManager = new OrdersDataManager();
            int count = 0;
            var allOrders = ordersDataManager.Select();
            if (allOrders.Where(x => x.personID == customer.nationalID).Any())
                count = allOrders.Where(x => x.personID == customer.nationalID).Count();

            if (count == 0)
                generalBonus = 5;
            if (count == 1)
                generalBonus = 10;

            if (generalBonus != 0)
                MessageBox.Show($"You have a general bonus of {generalBonus}%.", "Congratulations!");
        }

        // special bonus for each payment above 150, or for every 7 orders
        private void CheckForSpecialBonus()
        {
            SpecialBonuses specialBonuses = new SpecialBonuses();
            List<string> bonuses = specialBonuses.Select();
            if(bonuses.Contains(customer.nationalID))
            {
                OrdersDataManager ordersDataManager = new OrdersDataManager();
                int count = 0;
                var allOrders = ordersDataManager.Select();
                if (allOrders.Where(x => x.personID == customer.nationalID).Any())
                    count = allOrders.Where(x => x.personID == customer.nationalID).Count();

                if(count <= 5)
                {
                    specialBonus = 15;
                }
                else if(count <= 8)
                {
                    specialBonus = 22;
                }
                else if(count <= 12)
                {
                    specialBonus = 27;
                }
                else
                {
                    specialBonus = 34;
                }

                MessageBox.Show($"You have a special bonus of {specialBonus}%.", "Congratulations!");
            }
        }

        private void CartDataGrid_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            DependencyObject dep = e.GetRightClickDependencyObject();
            dep.SetContextItemsEnabality(SetCartContextItemsEnablity);
        }

        private void SetCartContextItemsEnablity(bool b)
        {
            RemoveContextItem.IsEnabled = b;
            AmountContextItem.IsEnabled = b;
        }

        private void DeleteCartContextItem_Click(object sender, RoutedEventArgs e)
        {
            string tableName = TableName.Carts.ToString().ToLower();
            string updatedFieldsInfo = "registerdate='', deliverdate='', foodstring=''";
            string criteriaString = $"personid='{customer.nationalID}'";

            App.db.Update(criteriaString, updatedFieldsInfo, tableName);
        }
    }
}
