﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FoodOrderingSystem
{
    /// <summary>
    /// Interaction logic for CustomerOrderWindow.xaml
    /// </summary>
    public partial class CustomerOrderWindow : Window
    {
        string[] orderTypes { get; }
        string currentOrderType = OrderType.All.ToString();
        List<OrderGrid> allCustomerOrders;
        DateTime startDate;
        DateTime endDate;
        OrdersDataManager ordersDataManager;
        Customer customer;
        public CustomerOrderWindow(Customer customer)
        {
            InitializeComponent();
            ordersDataManager = new OrdersDataManager();
            allCustomerOrders = ordersDataManager.ReadOrderData(customer.nationalID);
            orderTypes = new string[3] { OrderType.All.ToString(), OrderType.A.ToString(), OrderType.B.ToString()};
            //MessageBox.Show(allCustomerOrders.Select(x => x.foodData.Where(y => y.name == "Deleted").Select(z => z.name)).ToString());
            startDate = DateTime.Today;
            endDate = DateTime.Today.AddDays(1);
            this.customer = customer;
        }

        private void CancelOrderContextItem_Click(object sender, RoutedEventArgs e)
        {
            OrderGrid selectedOrderGrid = (OrderGrid)CustomerOrdersGrid.SelectedItem;
            selectedOrderGrid.CancelOrder(customer.nationalID);

            if (selectedOrderGrid.orderType == 'A')
            {
                MessageBox.Show
                    ($"90% of your money will be paid back to your account : {selectedOrderGrid.totalPrice * 0.9f} out of {selectedOrderGrid.totalPrice}",
                    "Money Widthrawal");
            }
            else
            {
                MessageBox.Show($"Please pay 10% of the total price online : {selectedOrderGrid.totalPrice * 0.1f} out of {selectedOrderGrid.totalPrice}",
                    "Money Widthdrawal");
            }

            // refresh data
            allCustomerOrders = ordersDataManager.ReadOrderData(customer.nationalID);
            Reset();
        }

        private void StartDateButton_Click(object sender, RoutedEventArgs e)
        {
            startDate = this.ChooseDate();
        }

        private void EndDateButton_Click(object sender, RoutedEventArgs e)
        {
            endDate = this.ChooseDate();
            endDate = endDate.AddDays(1);
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            LoadOrders(allCustomerOrders);
            OrderComboBox.ItemsSource = orderTypes.ToList();
            // MessageBox.Show(DateTime.Today.ToString());
        }

        private void LoadOrders(List<OrderGrid> list)
        {
            //var finalList = new List<Object>();
            //foreach(var item in list)
            //{
            //    finalList.Add(item);
            //    finalList.AddRange(item.foodData);
            //}
            list = list.OrderByDescending(x => x.orderDateTime.ToDateTime()).ToList();
            CustomerOrdersGrid.ItemsSource = list;
            CustomerOrdersGrid.IsTextSearchCaseSensitive = false;
            CustomerOrdersGrid.IsTextSearchEnabled = true;
        }

        private void ApplyButton_Click(object sender, RoutedEventArgs e)
        {
            currentOrderType = OrderComboBox.Text;
            // MessageBox.Show(currentOrderType[0].ToString());

            List<OrderGrid> tempList = new List<OrderGrid>();

            if (allCustomerOrders.Where(x => x.orderDateTime.CheckDateRange(startDate, endDate)).Any())
                tempList = allCustomerOrders.Where(x => x.orderDateTime.CheckDateRange(startDate, endDate)).ToList();

            if (currentOrderType != "All")
            {
                if(tempList.Where(x => x.orderType == currentOrderType[0]).Any())
                    tempList = tempList.Where(x => x.orderType == currentOrderType[0]).ToList();
            }

            // MessageBox.Show(startDate.ToString() + " -> " + endDate.ToString());
            LoadOrders(tempList);
        }

        private void ResetButton_Click(object sender, RoutedEventArgs e)
        {
            Reset();
        }

        private void Reset()
        {
            currentOrderType = OrderType.All.ToString();
            startDate = DateTime.Today;
            endDate = DateTime.Today;
            OrderComboBox.Text = "All";
            LoadOrders(allCustomerOrders);
        }

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            CustomerWindow customerWindow = new CustomerWindow(customer);
            this.Close();
            customerWindow.ShowDialog();
        }

        private void CustomerOrdersGrid_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            DependencyObject dep = e.GetRightClickDependencyObject();
            dep.SetContextItemsEnabality(b => CancelOrderContextItem.IsEnabled = b);
        }
    }
}
