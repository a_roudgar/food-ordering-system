﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FoodOrderingSystem
{
    /// <summary>
    /// Interaction logic for CommentWindow.xaml
    /// </summary>
    public partial class CommentWindow : Window
    {
        string code;
        bool isReadModeOn;
        Customer customer;
        public CommentWindow(string code, bool isReadModeOn, Customer customer)
        {
            InitializeComponent();
            this.code = code;
            this.isReadModeOn = isReadModeOn;
            this.customer = customer;
        }

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (isReadModeOn)
            {
                ApplyButton.IsEnabled = false;
                AnonymousCheckBox.Visibility = Visibility.Hidden;
                ShowComments();
            }
            else
            {
                CommentsTextBox.IsEnabled = true;
                ApplyButton.IsEnabled = true;
            }
                
        }

        private void ShowComments()
        {
            try
            {
                if (!File.Exists(App.opinionsFolder + code + ".txt"))
                    File.Create(App.opinionsFolder + code + ".txt");
                CommentsTextBox.Text = File.ReadAllText(App.opinionsFolder + code + ".txt");
            }
            catch
            {
                MessageBox.Show("An Error Occured while retrieving the comments on this food.", "CommentTimeError!");
            }
        }

        private void SaveComment()
        {
            DateTime today = DateTime.Today;
            string date = $"{today.Month}/{today.Day}/{today.Year} {today.Hour}:{today.Minute}";
            string initials = $"({date})";
            
            StreamWriter writer = null;
            try
            {               
                writer = new StreamWriter(App.opinionsFolder + code + ".txt", true);
                writer.WriteLine(initials);
                initials = AnonymousCheckBox.IsChecked == true ? "*Anonymous User*" : $"*{customer.name} {customer.familyName}*";
                string comment = CommentsTextBox.Text;
                writer.WriteLine(initials);
                writer.WriteLine(comment);
                writer.WriteLine();

                MessageBox.Show("Comment added successfully.");
            }
            catch
            {
                MessageBox.Show("A problem occured while trying to save your comment");
            }
            finally
            {
                if (writer != null)
                    writer.Close();
            }
        }

        private void ApplyButton_Click(object sender, RoutedEventArgs e)
        {
            SaveComment();
            this.Close();
        }
    }
}
