﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using MySql.Data.MySqlClient;

namespace FoodOrderingSystem
{
    public class Drawings : ISelect<string[]>
    {
        public List<string[]> Select()
        {
            string query = $"SELECT * FROM {TableName.Drawing.ToString().ToLower()}";

            // Create a List to store the result
            List<string[]> list = new List<string[]>();

            DBConnect myRestaurantDatabase = App.db;

            //Open connection
            if (myRestaurantDatabase.OpenConnection() == true)
            {
                //Create Command
                MySqlCommand cmd = new MySqlCommand(query, myRestaurantDatabase.sqlConnection);
                //Create a data reader and Execute the command
                MySqlDataReader dataReader = cmd.ExecuteReader();

                //Read the data and store them in the list
                while (dataReader.Read())
                {
                    list.Add(new string[] { dataReader["personid"] + "", dataReader["registerorderid"] + ""});
                }

                //close Data Reader
                dataReader.Close();

                //close connection
                myRestaurantDatabase.CloseConnection();

                //return list to be displayed
                return list;
            }
            else
            {
                return list;
            }
        }

        public void SaveToDrawings(string id, string ordercode)
        {
            if (!this.Select().Where(x => x[0] == id).Any())
            {
                Dictionary<string, string> dict = new Dictionary<string, string>()
                {
                    ["personid"] = $"'{id}'",
                    ["registerorderid"] = $"'{ordercode}'"
                };
                App.db.Insert(dict, TableName.Drawing.ToString().ToLower());
            }
        }

        private void DeleteFromDrawings(string id)
        {
            App.db.Delete($"personid='{id}'", TableName.Drawing.ToString().ToLower());
        }

        public List<string> Roll()
        {
            List<string> orderIDs = this.Select().Select(z => z[1]).ToList();
            List<Order> orders = new OrdersDataManager().Select();

            int x = new Random().Next(0, 10);
            List<string> winnerIDs = new List<string>();
            foreach (Order order in orders)
            {
                string priceString = order.totalPrice.ToString();
                int lastFigure = int.Parse(priceString[priceString.Length - 1].ToString());
                if (lastFigure == x && orderIDs.Contains(order.code))
                {                    
                    if(!winnerIDs.Contains(order.personID))
                    {
                        winnerIDs.Add(order.personID);
                        DeleteFromDrawings(order.personID);
                    }                    
                }
                    
            }

            return winnerIDs;
        }
    }
}
