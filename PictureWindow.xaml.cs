﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FoodOrderingSystem
{
    /// <summary>
    /// Interaction logic for PictureWindow.xaml
    /// </summary>
    public partial class PictureWindow : Window
    {
        public string imagefile { get; private set; }
        string foodCode;
        public PictureWindow(string imagefile, string foodCode, bool isEditModeEnabled)
        {
            InitializeComponent();
            this.imagefile = imagefile;
            this.foodCode = foodCode;
            if(isEditModeEnabled)
            {
                BrowseButton.Visibility = Visibility.Visible;
                SaveButton.Visibility = Visibility.Visible;
            }
        }

        private void BrowseButton_Click(object sender, RoutedEventArgs e)
        {
            FoodImage.BrowseImage();
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            string tableName = TableName.Foods.ToString().ToLower();
            string updatedFieldsInfo = $"imagefile='{FoodImage.Source}'";
            string criteriaString = $"id='{foodCode}'";

            App.db.Update(criteriaString, updatedFieldsInfo, tableName);
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            FoodImage.Source = new BitmapImage(new Uri(imagefile));
        }
    }
}
