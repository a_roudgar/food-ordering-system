﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FoodOrderingSystem
{
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {
        public readonly UserType userType;
        DBConnect myRestaurantDatabase;
        Admin admin = null;
        Customer customer = null;
        public LoginWindow(UserType userType)
        {
            InitializeComponent();
            this.userType = userType;
            if (userType == UserType.Admin)
                UserEmailTextBlock.Text = "Username:";
            else
            {
                UserEmailTextBlock.Text = "Email:";
                SignUpButton.IsEnabled = true;
            }       

          myRestaurantDatabase = App.db;
        }

        private void BackToWelcomeButton_Click(object sender, RoutedEventArgs e)
        {
            MainWindow mainWindow = new MainWindow();
            this.Close();
            mainWindow.ShowDialog();
        }

        private void LoginButton_Click(object sender, RoutedEventArgs e)
        {
            if(userType == UserType.Admin)
            {
                try
                {
                    admin = new AdminDataManager().Login(UserEmailTextBox.Text, LoginPasswordBox.Password);
                    AdminPage adminPage = new AdminPage(admin);
                    this.Close();
                    adminPage.ShowDialog();
                }
                catch(Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error!", MessageBoxButton.OK);
                    LoginPasswordBox.Password = "";
                }
            }
            else
            {
                try
                {
                    customer = new CustomerDataManager().Login(UserEmailTextBox.Text, LoginPasswordBox.Password);
                    CustomerWindow customerWindow = new CustomerWindow(customer);
                    this.Close();
                    customerWindow.ShowDialog();
                }
                catch(Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error!", MessageBoxButton.OK);
                    LoginPasswordBox.Password = "";
                }
            }

        }

        private void SignUpButton_Click(object sender, RoutedEventArgs e)
        {
            SignUpWindow signUpWindow = new SignUpWindow();
            this.Close();
            signUpWindow.ShowDialog();
        }
    }
}
