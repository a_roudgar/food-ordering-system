﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FoodOrderingSystem
{
    /// <summary>
    /// Interaction logic for RateWindow.xaml
    /// </summary>
    public partial class RateWindow : Window
    {
        Image star = new Image();
        Image blackStar = new Image();
        public Customer customer { get; private set; }
        Food food;
        int score = 1;
        public RateWindow(Customer customer, Food food)
        {
            InitializeComponent();
            this.customer = customer;
            this.food = food;
            star.Source = new BitmapImage(new Uri(App.currentProjectPath + @"\Images\star.jfif"));
            blackStar.Source = new BitmapImage(new Uri(App.currentProjectPath + @"\Images\blackstar.jfif"));
        }

        private void AcceptButton_Click(object sender, RoutedEventArgs e)
        {           

            string tableName = TableName.Customers.ToString().ToLower();
            string criteriaString = $"nationalid='{customer.nationalID}'";
            string updatedFieldsInfo = "votestring='";
            foreach (KeyValuePair<string, int> item in customer.codeVote)
            {
                if (item.Key != food.code)
                    updatedFieldsInfo += $"{item.Key}-{item.Value},";
            }              
            updatedFieldsInfo += $"{food.code}-{score},'";

            // save new scoring data for customer
            App.db.Update(criteriaString, updatedFieldsInfo, tableName);

            // calculating new score and vote for food
            float foodScore = food.score;
            int voteCount = food.vote;
            if (!customer.codeVote.ContainsKey(food.code))
            {
                voteCount++;
                foodScore = (foodScore * voteCount + score) / (voteCount);
            }                
            else
                foodScore = (foodScore * voteCount - customer.codeVote[food.code] + score) / voteCount;
            
            // save new vote and score info for food
            App.db.Update($"id='{food.code}'", $"score={foodScore}, vote={voteCount}", TableName.Foods.ToString().ToLower());

            // refresh customer info
            customer = new CustomerDataManager().Select().Where(x => x.nationalID == customer.nationalID).First();
            
            this.Close();
        }

        private void CacnelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Check1_Checked(object sender, RoutedEventArgs e)
        {
            Check1.IsChecked = true;
            Check2.IsChecked = false;
            Check3.IsChecked = false;
            Check4.IsChecked = false;
            Check5.IsChecked = false;

            StarOne.Source = star.Source;
            StarTwo.Source = blackStar.Source;
            StarThree.Source = blackStar.Source;
            StarFour.Source = blackStar.Source;
            StarFive.Source = blackStar.Source;

            score = 1;
        }

        private void Check2_Checked(object sender, RoutedEventArgs e)
        {
            Check1.IsChecked = false;
            Check2.IsChecked = true;
            Check3.IsChecked = false;
            Check4.IsChecked = false;
            Check5.IsChecked = false;

            StarOne.Source = star.Source;
            StarTwo.Source = star.Source;
            StarThree.Source = blackStar.Source;
            StarFour.Source = blackStar.Source;
            StarFive.Source = blackStar.Source;

            score = 2;
        }

        private void Check3_Checked(object sender, RoutedEventArgs e)
        {
            Check1.IsChecked = false;
            Check2.IsChecked = false;
            Check3.IsChecked = true;
            Check4.IsChecked = false;
            Check5.IsChecked = false;

            StarOne.Source = star.Source;
            StarTwo.Source = star.Source;
            StarThree.Source = star.Source;
            StarFour.Source = blackStar.Source;
            StarFive.Source = blackStar.Source;

            score = 3;
        }

        private void Check4_Checked(object sender, RoutedEventArgs e)
        {
            Check1.IsChecked = false;
            Check2.IsChecked = false;
            Check3.IsChecked = false;
            Check4.IsChecked = true;
            Check5.IsChecked = false;

            StarOne.Source = star.Source;
            StarTwo.Source = star.Source;
            StarThree.Source = star.Source;
            StarFour.Source = star.Source;
            StarFive.Source = blackStar.Source;

            score = 4;
        }

        private void Check5_Checked(object sender, RoutedEventArgs e)
        {
            Check1.IsChecked = false;
            Check2.IsChecked = false;
            Check3.IsChecked = false;
            Check4.IsChecked = false;
            Check5.IsChecked = true;

            StarOne.Source = star.Source;
            StarTwo.Source = star.Source;
            StarThree.Source = star.Source;
            StarFour.Source = star.Source;
            StarFive.Source = star.Source;

            score = 5;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Check1.IsChecked = true;
            Check2.IsChecked = false;
            Check3.IsChecked = false;
            Check4.IsChecked = false;
            Check5.IsChecked = false;

            StarOne.Source = star.Source;
            StarTwo.Source = blackStar.Source;
            StarThree.Source = blackStar.Source;
            StarFour.Source = blackStar.Source;
            StarFive.Source = blackStar.Source;

            score = 1;
        }
    }
}
