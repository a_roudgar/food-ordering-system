﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FoodOrderingSystem
{
    /// <summary>
    /// Interaction logic for SignatureWindow.xaml
    /// </summary>
    public partial class SignatureWindow : Window
    {
        Customer customer;
        List<CartGrid> cartList;
        int finalBonus;
        string deliverDate;
        Point currentPoint;
        public SignatureWindow(Customer customer, List<CartGrid> cartList, int finalBonus, string deliverDate)
        {
            InitializeComponent();
            currentPoint = new Point();
            this.customer = customer;
            this.cartList = cartList;
            this.finalBonus = finalBonus;
            this.deliverDate = deliverDate;
        }

        private void ApplyButton_Click(object sender, RoutedEventArgs e)
        {
            if(SignatureCanvas.Strokes.Count != 0)
            {
                //string address = App.currentProjectPath + @"\Signatures\" + customer.nationalID + ".png";
                //if(File.Exists(address) && File.)
                //    address.DeleteFile();
                SignatureCanvas.SaveSignature(customer.nationalID);
                BillWindow billWindow = new BillWindow(customer, cartList, finalBonus, deliverDate);
                this.Close();
                billWindow.ShowDialog();
            }
            else
            {
                MessageBox.Show("You must first sign.", "SignatureError!");
            }
            
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void SignatureCanvas_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ButtonState == MouseButtonState.Pressed)
                currentPoint = e.GetPosition(this);
        }

        private void SignatureCanvas_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                Line line = new Line();

                line.Stroke = SystemColors.WindowFrameBrush;
                line.X1 = currentPoint.X;
                line.Y1 = currentPoint.Y;
                line.X2 = e.GetPosition(this).X;
                line.Y2 = e.GetPosition(this).Y;

                currentPoint = e.GetPosition(this);

            }
        }

        private void EraseButton_Click(object sender, RoutedEventArgs e)
        {
            SignatureCanvas.Strokes.Clear();
        }
    }
}
