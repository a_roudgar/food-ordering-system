﻿using System;
using System.Linq;
using System.Windows;
using System.IO;

namespace FoodOrderingSystem
{
    /// <summary>
    /// Interaction logic for AdminPersonalInfo.xaml
    /// </summary>
    public partial class AdminPersonalInfo : Window
    {
        public Admin admin { get; private set; }
        AdminDataManager adminDataManager;
        public AdminPersonalInfo(Admin admin)
        {
            InitializeComponent();
            this.admin = admin;
            adminDataManager = new AdminDataManager();
        }

        private void BrowseImageButton_Click(object sender, RoutedEventArgs e)
        {
            PersonalImage.BrowseImage();
        }

        private void ResetButton_Click(object sender, RoutedEventArgs e)
        {
            EmailTextBox.Text = admin.email;
            FirstNameTextBox.Text = admin.name;
            LastNameTextBox.Text = admin.familyName;
            PersonalImage.LoadImage(admin.image);
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void SaveInfoButton_Click(object sender, RoutedEventArgs e)
        {
            string tableName = TableName.Admins.ToString().ToLower();
            string criteriaString = $"username='{UsernameTextBox.Text}'";            
            string updatedFieldsInfo = "";
            try
            {
                AdminDataManager testRepetitive = new AdminDataManager();
                if (testRepetitive.Select().Where(x => x.email == EmailTextBox.Text).Any())
                {
                    if(testRepetitive.Select().Where(x => x.email == EmailTextBox.Text).First().userName != admin.userName)
                        throw new ArgumentException("This email is already being used by another admin.");
                }

                // might throw an exception
                testRepetitive.CanUpdate(EmailTextBox.Text, FirstNameTextBox.Text, LastNameTextBox.Text);               

                updatedFieldsInfo += $"firstname='{FirstNameTextBox.Text}', lastname='{LastNameTextBox.Text}', email='{EmailTextBox.Text}'";
                if (PersonalImage.Source != null)
                {
                    updatedFieldsInfo += $",imagefile='{PersonalImage.Source.ToString()}'";
                }
                else
                {
                    updatedFieldsInfo += ",imagefile=''";
                }
                    

                App.db.Update(criteriaString, updatedFieldsInfo, tableName);
                admin = adminDataManager.Select().Where(x => x.nationalID == admin.nationalID).First();
                this.Close();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!");
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            // setting predefined info
            UsernameTextBox.Text = admin.userName;
            LoginCountsTextBox.Text = admin.loginCount.ToString();
            FirstNameTextBox.Text = admin.name;
            LastNameTextBox.Text = admin.familyName;
            NationalIDTextBox.Text = admin.nationalID;
            EmailTextBox.Text = admin.email;

            // read restaurant phone number
            string fileAddress = App.restaurantInfoFile;
            if(File.Exists(fileAddress))
            {
                StreamReader reader = null;
                try
                {
                    reader = new StreamReader(fileAddress);
                    string line = "";
                    while (reader.EndOfStream == false)
                        line = reader.ReadLine();

                    PhoneTextBox.Text = line;
                }
                catch
                {
                    MessageBox.Show("Can not read restaurant phone number", "FileError!");
                }
                finally
                {
                    if (reader != null)
                        reader.Close();
                }
            }

            try
            {
                // reading image address
                PersonalImage.LoadImage(admin.image);
            }
            catch
            {
                MessageBox.Show("Can't Load Image.", "ImageError!");
            }
            
        }
    }
}
