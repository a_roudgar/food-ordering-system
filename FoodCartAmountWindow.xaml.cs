﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FoodOrderingSystem
{
    /// <summary>
    /// Interaction logic for FoodCartAmountWindow.xaml
    /// </summary>
    public partial class FoodCartAmountWindow : Window
    {
        Food selectedFood;
        string personID;
        string deliverDate;
        int maxAmount;
        CalendarDataManager calendarDataManager; 
        public FoodCartAmountWindow(Food selectedFood, string personID, string deliverDate)
        {
            InitializeComponent();
            this.selectedFood = selectedFood;
            this.personID = personID;
            this.deliverDate = deliverDate;
            calendarDataManager = new CalendarDataManager();
            maxAmount = calendarDataManager.readDayData(deliverDate).Where(x => x.code == selectedFood.code).Select(x => x.count).First();
        }

        private void ApplyFoodAmountButton_Click(object sender, RoutedEventArgs e)
        {
            CartDataManager cartDataManager = new CartDataManager();

            Cart currentCart = cartDataManager.FindPersonCart(personID);
            try
            {               
                string todayDate = $"{DateTime.Today.Month}/{DateTime.Today.Day}/{DateTime.Today.Year}";
                
                // this line might throw an exception
                int amount = int.Parse(AmountTextBox.Text);
                if (amount > maxAmount)
                    throw new ArgumentOutOfRangeException($"The Max Amount that you can use is: {maxAmount}");
                if (amount <= 0)
                    throw new ArgumentException("Input should be an integer greater than zero.");

                string tableName = TableName.Carts.ToString().ToLower();
                if (currentCart.registerDate != todayDate)
                {
                    string criteriaString = $"personid='{personID}'";
                    string updatedFieldsInfo = $"registerdate='{todayDate}', foodstring='{selectedFood.code}-{amount},'," +
                        $" deliverdate='{deliverDate}'"; 

                    App.db.Update(criteriaString, updatedFieldsInfo, tableName);
                }
                else
                {
                    if(deliverDate != currentCart.deliverDate)
                    {
                        string criteriaString = $"personid='{personID}'";
                        string updatedFieldsInfo = $"registerdate='{todayDate}', foodstring='{selectedFood.code}-{amount},'," +
                            $" deliverdate='{deliverDate}'";

                        App.db.Update(criteriaString, updatedFieldsInfo, tableName);
                    }
                    else
                    {
                        string criteriaString = $"personid='{personID}'";
                        string updatedFieldsInfo = "foodstring='";
                        CartFoods cartFoodsData = cartDataManager.ReadCartData(personID);
                        if(cartFoodsData != null)
                        {
                            foreach(KeyValuePair<Food, int> foodAmountPair in cartFoodsData.cartFoodData)
                            {
                                if(foodAmountPair.Key.code != selectedFood.code)
                                {
                                    updatedFieldsInfo += $"{foodAmountPair.Key.code}-{foodAmountPair.Value},";
                                }
                            }
                        }

                        updatedFieldsInfo += $"{selectedFood.code}-{amount},',";
                        updatedFieldsInfo += $"registerdate='{todayDate}', deliverdate='{deliverDate}'";
                        App.db.Update(criteriaString, updatedFieldsInfo, tableName);
                    }
                }

                this.Close();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message + $" paramName = {ex.TargetSite} ", "AddToCartTimeError!");
            }
            
        }

        private void CancelFoodAmountButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            AmountTextBlock.Text += $"\nMax = {maxAmount}";
        }
    }
}
