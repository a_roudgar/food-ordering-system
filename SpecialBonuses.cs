﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace FoodOrderingSystem
{
    class SpecialBonuses : ISelect<string>
    {
        public List<string> Select()
        {
            string query = $"SELECT * FROM {TableName.SpecialBonus.ToString().ToLower()}";

            // Create a List to store the result
            List<string> list = new List<string>();

            DBConnect myRestaurantDatabase = App.db;

            //Open connection
            if (myRestaurantDatabase.OpenConnection() == true)
            {
                //Create Command
                MySqlCommand cmd = new MySqlCommand(query, myRestaurantDatabase.sqlConnection);
                //Create a data reader and Execute the command
                MySqlDataReader dataReader = cmd.ExecuteReader();

                //Read the data and store them in the list
                while (dataReader.Read())
                {
                    list.Add(dataReader["personid"] + "");
                }

                //close Data Reader
                dataReader.Close();

                //close connection
                myRestaurantDatabase.CloseConnection();

                //return list to be displayed
                return list;
            }
            else
            {
                return list;
            }
        }

        public void SaveToBonusList(string id)
        {
            if (!this.Select().Where(x => x == id).Any())
            {
                App.db.Insert(new Dictionary<string, string>() { ["personid"] = $"'{id}'" }, TableName.SpecialBonus.ToString().ToLower());
            }
        }

        public void DeleteFromBonusList(string id)
        {
            if (this.Select().Where(x => x == id).Any())
            {
                App.db.Delete($"personid='{id}'", TableName.SpecialBonus.ToString().ToLower());
            }
        }
    }
}
