﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FoodOrderingSystem
{
    /// <summary>
    /// Interaction logic for SignUpWindow.xaml
    /// </summary>
    public partial class SignUpWindow : Window
    {
        public SignUpWindow()
        {
            InitializeComponent();
        }

        private void BackToLoginButton_Click(object sender, RoutedEventArgs e)
        {
            LoginWindow loginWindow = new LoginWindow(UserType.Customer);
            this.Close();
            loginWindow.ShowDialog();
        }

        private void ResetSignUpButton_Click(object sender, RoutedEventArgs e)
        {
            AddressTextBox.Text = "";
            EmailTextBox.Text = "";
            FirstNameTextBox.Text = "";
            LastNameTextBox.Text = "";
            NationalIDTextBox.Text = "";
            PhoneNumberTextBox.Text = "";
            SignUpPasswordBox.Password = "";
            ConfirmPasswordBox.Password = "";
        }

        private void RegisterCustomerButton_Click(object sender, RoutedEventArgs e)
        {
            if(AddressTextBox.Text == "" || EmailTextBox.Text == "" || FirstNameTextBox.Text == "" || LastNameTextBox.Text == "" || NationalIDTextBox.Text == "" ||
                PhoneNumberTextBox.Text == "" || SignUpPasswordBox.Password == "" || ConfirmPasswordBox.Password == "")
            {
                MessageBox.Show("Every field must be filled with proper values.\nNo field can remain empty.", "Error!", MessageBoxButton.OK);
                SignUpPasswordBox.Password = "";
                ConfirmPasswordBox.Password = "";
                return;
            }

            if(SignUpPasswordBox.Password != ConfirmPasswordBox.Password)
            {
                MessageBox.Show("Confirm your password again.", "Error!", MessageBoxButton.OK);
                SignUpPasswordBox.Password = "";
                ConfirmPasswordBox.Password = "";
                return;
            }

            try
            {
                new CustomerDataManager().SignUp
                    (FirstNameTextBox.Text, LastNameTextBox.Text, AddressTextBox.Text, PhoneNumberTextBox.Text, EmailTextBox.Text, NationalIDTextBox.Text, SignUpPasswordBox.Password);

                new CartDataManager().AddCart(NationalIDTextBox.Text);

                LoginWindow loginWindow = new LoginWindow(UserType.Customer);
                this.Close();
                loginWindow.ShowDialog();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Register Error!", MessageBoxButton.OK);
                SignUpPasswordBox.Password = "";
                ConfirmPasswordBox.Password = "";
            }
               
        }
    }
}
