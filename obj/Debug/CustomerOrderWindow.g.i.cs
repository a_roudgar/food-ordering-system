﻿#pragma checksum "..\..\CustomerOrderWindow.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "2417EAE55D409B63972BDE3579D6FCBC6E6D81EC581BC6F4E6B6B93713DA0C97"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using FoodOrderingSystem;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace FoodOrderingSystem {
    
    
    /// <summary>
    /// CustomerOrderWindow
    /// </summary>
    public partial class CustomerOrderWindow : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 15 "..\..\CustomerOrderWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid CustomerOrdersGrid;
        
        #line default
        #line hidden
        
        
        #line 31 "..\..\CustomerOrderWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.MenuItem CancelOrderContextItem;
        
        #line default
        #line hidden
        
        
        #line 52 "..\..\CustomerOrderWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button ApplyButton;
        
        #line default
        #line hidden
        
        
        #line 56 "..\..\CustomerOrderWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button ResetButton;
        
        #line default
        #line hidden
        
        
        #line 60 "..\..\CustomerOrderWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BackButton;
        
        #line default
        #line hidden
        
        
        #line 72 "..\..\CustomerOrderWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button StartDateButton;
        
        #line default
        #line hidden
        
        
        #line 75 "..\..\CustomerOrderWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button EndDateButton;
        
        #line default
        #line hidden
        
        
        #line 90 "..\..\CustomerOrderWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox OrderComboBox;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/FoodOrderSystem;component/customerorderwindow.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\CustomerOrderWindow.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 8 "..\..\CustomerOrderWindow.xaml"
            ((FoodOrderingSystem.CustomerOrderWindow)(target)).Loaded += new System.Windows.RoutedEventHandler(this.Window_Loaded);
            
            #line default
            #line hidden
            return;
            case 2:
            this.CustomerOrdersGrid = ((System.Windows.Controls.DataGrid)(target));
            
            #line 21 "..\..\CustomerOrderWindow.xaml"
            this.CustomerOrdersGrid.MouseRightButtonUp += new System.Windows.Input.MouseButtonEventHandler(this.CustomerOrdersGrid_MouseRightButtonUp);
            
            #line default
            #line hidden
            return;
            case 3:
            this.CancelOrderContextItem = ((System.Windows.Controls.MenuItem)(target));
            
            #line 32 "..\..\CustomerOrderWindow.xaml"
            this.CancelOrderContextItem.Click += new System.Windows.RoutedEventHandler(this.CancelOrderContextItem_Click);
            
            #line default
            #line hidden
            return;
            case 4:
            this.ApplyButton = ((System.Windows.Controls.Button)(target));
            
            #line 53 "..\..\CustomerOrderWindow.xaml"
            this.ApplyButton.Click += new System.Windows.RoutedEventHandler(this.ApplyButton_Click);
            
            #line default
            #line hidden
            return;
            case 5:
            this.ResetButton = ((System.Windows.Controls.Button)(target));
            
            #line 57 "..\..\CustomerOrderWindow.xaml"
            this.ResetButton.Click += new System.Windows.RoutedEventHandler(this.ResetButton_Click);
            
            #line default
            #line hidden
            return;
            case 6:
            this.BackButton = ((System.Windows.Controls.Button)(target));
            
            #line 61 "..\..\CustomerOrderWindow.xaml"
            this.BackButton.Click += new System.Windows.RoutedEventHandler(this.BackButton_Click);
            
            #line default
            #line hidden
            return;
            case 7:
            this.StartDateButton = ((System.Windows.Controls.Button)(target));
            
            #line 72 "..\..\CustomerOrderWindow.xaml"
            this.StartDateButton.Click += new System.Windows.RoutedEventHandler(this.StartDateButton_Click);
            
            #line default
            #line hidden
            return;
            case 8:
            this.EndDateButton = ((System.Windows.Controls.Button)(target));
            
            #line 75 "..\..\CustomerOrderWindow.xaml"
            this.EndDateButton.Click += new System.Windows.RoutedEventHandler(this.EndDateButton_Click);
            
            #line default
            #line hidden
            return;
            case 9:
            this.OrderComboBox = ((System.Windows.Controls.ComboBox)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

