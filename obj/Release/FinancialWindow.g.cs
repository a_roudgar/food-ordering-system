﻿#pragma checksum "..\..\FinancialWindow.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "852238B5F0CE1B2610AC0813420034493DAC52BA35AFAD6EB9A20C19D3DA51A6"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using FoodOrderingSystem;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace FoodOrderingSystem {
    
    
    /// <summary>
    /// FinancialWindow
    /// </summary>
    public partial class FinancialWindow : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 16 "..\..\FinancialWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid FinancialDataGrid;
        
        #line default
        #line hidden
        
        
        #line 30 "..\..\FinancialWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock TotalExpenseTextBlock;
        
        #line default
        #line hidden
        
        
        #line 35 "..\..\FinancialWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock TotalSaleTextBlock;
        
        #line default
        #line hidden
        
        
        #line 39 "..\..\FinancialWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock BenefitTextBlock;
        
        #line default
        #line hidden
        
        
        #line 62 "..\..\FinancialWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button StartDateButton;
        
        #line default
        #line hidden
        
        
        #line 70 "..\..\FinancialWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button EndDateButton;
        
        #line default
        #line hidden
        
        
        #line 79 "..\..\FinancialWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button ApplyButton;
        
        #line default
        #line hidden
        
        
        #line 88 "..\..\FinancialWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox OrderTypeComboBox;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/FoodOrderingSystem;component/financialwindow.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\FinancialWindow.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 8 "..\..\FinancialWindow.xaml"
            ((FoodOrderingSystem.FinancialWindow)(target)).Loaded += new System.Windows.RoutedEventHandler(this.Window_Loaded);
            
            #line default
            #line hidden
            
            #line 8 "..\..\FinancialWindow.xaml"
            ((FoodOrderingSystem.FinancialWindow)(target)).Closed += new System.EventHandler(this.Window_Closed);
            
            #line default
            #line hidden
            return;
            case 2:
            this.FinancialDataGrid = ((System.Windows.Controls.DataGrid)(target));
            return;
            case 3:
            this.TotalExpenseTextBlock = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 4:
            this.TotalSaleTextBlock = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 5:
            this.BenefitTextBlock = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 6:
            this.StartDateButton = ((System.Windows.Controls.Button)(target));
            
            #line 62 "..\..\FinancialWindow.xaml"
            this.StartDateButton.Click += new System.Windows.RoutedEventHandler(this.StartDateButton_Click);
            
            #line default
            #line hidden
            return;
            case 7:
            this.EndDateButton = ((System.Windows.Controls.Button)(target));
            
            #line 70 "..\..\FinancialWindow.xaml"
            this.EndDateButton.Click += new System.Windows.RoutedEventHandler(this.EndDateButton_Click);
            
            #line default
            #line hidden
            return;
            case 8:
            this.ApplyButton = ((System.Windows.Controls.Button)(target));
            
            #line 80 "..\..\FinancialWindow.xaml"
            this.ApplyButton.Click += new System.Windows.RoutedEventHandler(this.ApplyButton_Click);
            
            #line default
            #line hidden
            return;
            case 9:
            this.OrderTypeComboBox = ((System.Windows.Controls.ComboBox)(target));
            
            #line 90 "..\..\FinancialWindow.xaml"
            this.OrderTypeComboBox.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.OrderTypeComboBox_SelectionChanged);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

