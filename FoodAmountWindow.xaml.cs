﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FoodOrderingSystem
{
    /// <summary>
    /// Interaction logic for FoodAmountWindow.xaml
    /// </summary>
    public partial class FoodAmountWindow : Window
    {
        Food selectedFood { get; }
        string day { get; }
        public FoodAmountWindow(Food selectedFood, string day)
        {
            InitializeComponent();
            this.selectedFood = selectedFood;
            this.day = day;
        }

        private void ApplyFoodAmountButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int inputAmount = int.Parse(AmountTextBox.Text);
                if (inputAmount <= 0)
                    throw new ArgumentException("0 or less is not accepted.");
                UpdateDayFood(inputAmount);
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "InputError!", MessageBoxButton.OK);
            }
            finally
            {
                this.Close();
            }
        }

        private void UpdateDayFood(int amount)
        {
            CalendarDataManager calendarDataManager = new CalendarDataManager();
            string criteriaString = $"datestring='{day}'";
            string updatedFieldsInfo = "";
            updatedFieldsInfo += $"dayfoodstring='{selectedFood.code}-{amount}";
            var searchForPreSaved = calendarDataManager.readDayData(day);
            if (searchForPreSaved.Where(x => x.code == selectedFood.code).Any())
                calendarDataManager.UpdateCalendar(day, selectedFood, amount);
            else
            {
                updatedFieldsInfo += "'";

                string tableName = TableName.Calendar.ToString().ToLower();

                // save data
                App.db.Update(criteriaString, updatedFieldsInfo, tableName);
            }
                

        }

        private void CancelFoodAmountButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
