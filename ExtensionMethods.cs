﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media.Imaging;
using Microsoft.Win32;
using System.IO;
using System.Windows.Ink;
using System.Windows.Documents;
using System.Windows.Media;
using System.Reflection.Emit;

namespace FoodOrderingSystem
{
    public static class ExtensionMethods
    {
        // this method checks wether a given string represents a valid national id.
        // returns an exception if the string is null, empty or non-numeric.
        public static bool CheckNationalID(this string nationalID)
        {
            if (nationalID == null)
            {
                throw new ArgumentNullException();
            }
            if (nationalID == "")
            {
                throw new ArgumentException();
            }
            if (nationalID.Length != 10)
            {
                return false;
            }
            for (int i = 0; i < 10; i++)
            {
                try
                {
                    int num = int.Parse(nationalID[i].ToString());
                }
                catch
                {
                    throw new Exception("Your given national code is not a numeric value");
                }
            }
            int a = int.Parse(nationalID[9].ToString());
            int b = 0;
            for (int i = 0; i < 9; i++)
            {
                b += int.Parse(nationalID[i].ToString()) * (10 - i);
            }
            int c = b % 11;

            int countEquality = 1;
            int temp = a;
            for (int i = 1; i < 10; i++)
            {
                int t = int.Parse(nationalID[i].ToString());
                if (t == temp)
                    countEquality += 1;
                temp = t;
            }

            if (countEquality == 10)
                return false;
            if (a == c && c == 0)
                return true;
            if (c == 1 && a == 1)
                return true;
            if (c > 1 && a == Math.Abs(c - 11))
                return true;

            return false;
        }

        public static List<Food> SearchMenu(this List<Food> list, Func<Food, bool> criteria)
        {
            bool isNull = list.Where(criteria).Any();
            if (isNull)
            {
                list = list.Where(criteria).ToList();
                return list;
            }
            return new List<Food>();
        }

        public static bool CheckDateRange(this string date, DateTime startDate, DateTime endDate)
        {
            DateTime? comparingDate = date.ToDateTime();

            if (comparingDate != null)
            {
                if (comparingDate < endDate && comparingDate > startDate)
                    return true;
            }
            return false;
        }

        public static DateTime? ToDateTime(this string date)
        {
            try
            {
                return DateTime.Parse(date);
            }
            catch
            {
                return null;
            }
        }

        public static DateTime? ToDateOnly(this string date)
        {
            string[] dateSpecs = date.Split('/');
            string temp = "";
            for(int i = 0; i < 4; i++)
            {
                temp += dateSpecs[2][i].ToString();
            }
            dateSpecs[2] = temp;

            DateTime? resultDate = null;
            try
            {
                resultDate = new DateTime(int.Parse(dateSpecs[2]), int.Parse(dateSpecs[0]), int.Parse(dateSpecs[1]), 0, 0, 0);
            }
            catch
            {
                MessageBox.Show($"Problem with converting string to date: {date}");
                return null;
            }
            return resultDate;
        }

        public static DateTime ChooseDate(this Window window)
        {
            ChooseDateWindow chooseDateWindow = new ChooseDateWindow();
            window.IsEnabled = false;
            chooseDateWindow.ShowDialog();
            window.IsEnabled = true;
            return chooseDateWindow.date;
        }

        public static void LoadImage(this Image imageControl, string address)
        {
            Uri fileUri = address != "" ? new Uri(address) : null;
            if (fileUri != null)
                imageControl.Source = new BitmapImage(fileUri);
            else
                imageControl.Source = null;
        }

        public static void BrowseImage(this Image imageControl)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Multiselect = false;
            openFileDialog.Filter = "Images|*.png;*.bmp;*.jpg;*.jfif";
            openFileDialog.InitialDirectory = App.currentProjectPath;
            if (openFileDialog.ShowDialog() == true)
            {
                Uri fileUri = new Uri(openFileDialog.FileName);
                imageControl.Source = new BitmapImage(fileUri);
            }
        }

        public static bool CancelOrder(this OrderGrid selectedOrderGrid, string personID)
        {
            OrdersDataManager ordersDataManager = new OrdersDataManager();
            FoodDataManager foodDataManager = new FoodDataManager();
            CalendarDataManager calendarDataManager = new CalendarDataManager();
            List<Food> foodList = foodDataManager.Select();
            if (selectedOrderGrid.deliverDate.ToDateTime() <= DateTime.Now)
            {
                MessageBox.Show("This order is already delivered. No cancelation possible.");
                return false;
            }
            else
            {
                SpecialBonuses specialBonuses = new SpecialBonuses();
                if (specialBonuses.Select().Where(x => x == personID).Any())
                {
                    App.db.Delete($"personid='{personID}'", TableName.SpecialBonus.ToString().ToLower());
                }

                Drawings drawings = new Drawings();
                var drawingData = drawings.Select();
                if (drawingData.Where(x => x[0] == personID).Any())
                {
                    if (drawingData.Where(x => x[0] == personID).First()[1] == selectedOrderGrid.code)
                        App.db.Delete($"personid='{personID}'", TableName.Drawing.ToString().ToLower());
                }

                // update food amount in date
                List<MainFoodData> mainFoodDataList
                    = ordersDataManager
                    .ReadOrderData(personID)
                    .Where(x => x.code == selectedOrderGrid.code)
                    .Select(x => x.foodData).First();

                // delete data from database
                App.db.Delete($"id='{selectedOrderGrid.code}'", TableName.Orders.ToString().ToLower());

                DateTime deliverDateOnly = (DateTime)selectedOrderGrid.deliverDate.ToDateOnly();
                string date = $"{deliverDateOnly.Month}/{deliverDateOnly.Day}/{deliverDateOnly.Year}";

                foreach (var mainFoodData in mainFoodDataList)
                {
                    Food food = foodList.Where(x => x.code == mainFoodData.code).First();
                    int currentAmount = calendarDataManager.readDayData(date).Where(x => x.code == mainFoodData.code).Select(x => x.count).First();
                    calendarDataManager.UpdateCalendar(date, food, mainFoodData.amount + currentAmount);
                }

                return true;
            }
        }

        public static void SaveSignature(this InkCanvas signature, string filename)
        {
            string address = App.currentProjectPath + @"\Signatures\" + filename + ".png";

            Rect bounds = VisualTreeHelper.GetDescendantBounds(signature);
            double dpi = 96d;

            RenderTargetBitmap rtb = new RenderTargetBitmap((int)bounds.Width, (int)bounds.Height, dpi, dpi, PixelFormats.Default);
            DrawingVisual dv = new DrawingVisual();
            using (DrawingContext dc = dv.RenderOpen())
            {
                VisualBrush vb = new VisualBrush(signature);
                dc.DrawRectangle(vb, null, new Rect(new Point(), bounds.Size));
            }
            rtb.Render(dv);

            BitmapEncoder pngEncoder = new PngBitmapEncoder();
            pngEncoder.Frames.Add(BitmapFrame.Create(rtb));
            using (MemoryStream ms = new MemoryStream())
            {
                pngEncoder.Save(ms);

                FileStream fs = null;
                try
                {
                    fs = new FileStream(address, FileMode.Create, FileAccess.Write);
                    byte[] byteArray = ms.ToArray();
                    foreach (byte msByte in byteArray)
                        fs.WriteByte(msByte);
                }
                catch
                {
                    MessageBox.Show("A problem occured, please try again.", "SignatureSaveError!");
                }
                finally
                {
                    if (fs != null)
                        fs.Close();
                }
                
                //ms.Close();
            }
        }

        public static void DeleteFile(this string address)
        {
            try
            {
                File.Delete(address);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "DeleteFileError!");
            }
        }

        public static void SaveImage(this Image signature, string filename)
        {
            string address = App.currentProjectPath + @"\Signatures\" + filename + ".png";

            RenderTargetBitmap rtb = new RenderTargetBitmap((int)signature.Width, (int)signature.Height, 96d, 96d, PixelFormats.Default);
            rtb.Render(signature);
            FileStream fs = new FileStream(address, FileMode.Create);
            PngBitmapEncoder encoder = new PngBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(rtb));
            encoder.Save(fs);
            if (fs != null)
                fs.Close();

            App.db.Update($"id='{filename}'", $"signatureaddress='{address}'", TableName.Orders.ToString().ToLower());
        }

        public static void ReadSignature(this Image signature, string filename)
        {
            string address = App.currentProjectPath + @"\Signatures\" + @filename + ".png";
            signature.Source = new BitmapImage(new Uri(address));
        }

        public static DependencyObject GetRightClickDependencyObject(this MouseButtonEventArgs e)
        {
            DependencyObject dep = (DependencyObject)e.OriginalSource;
            while (dep != null && !(dep is DataGridCell) && !(dep is DataGridColumnHeader))
                dep = VisualTreeHelper.GetParent(dep);

            return dep;
        }

        public static void SetContextItemsEnabality(this DependencyObject dep, Action<bool> action)
        {
            if (dep is DataGridCell)
            {
                action(true);
            }
            else if (dep == null || dep is DataGridColumnHeader)
            {
                action(false);
            }
        }
    }
}
