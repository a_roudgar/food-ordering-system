﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace FoodOrderingSystem
{
    class Cart
    {
        public string personID { get; }
        public string foodString { get; } // containing food code and amount
        public string deliverDate { get; }
        public string registerDate { get; }
        public Cart(string personID, string foodString, string deliverDate, string registerDate)
        {
            this.personID = personID;
            this.foodString = foodString;
            this.deliverDate = deliverDate;
            this.registerDate = registerDate;
        }      
    }

    class CartDataManager : ISelect<Cart>
    {
        public void AddCart(string personID)
        {
            Dictionary<string, string> dict = new Dictionary<string, string>()
            {
                ["personid"] = $"'{personID}'",
                ["foodstring"] = "''",
                ["deliverdate"] = "''",
                ["registerdate"] = "''"
            };

            App.db.Insert(dict, TableName.Carts.ToString().ToLower());
        }

        // Select statement
        // this one will select all carts
        public List<Cart> Select()
        {
            string query = $"SELECT * FROM {TableName.Carts.ToString().ToLower()}";

            // Create a List to store the result
            List<Cart> list = new List<Cart>();

            DBConnect myRestaurantDatabase = App.db;

            //Open connection
            if (myRestaurantDatabase.OpenConnection() == true)
            {
                //Create Command
                MySqlCommand cmd = new MySqlCommand(query, myRestaurantDatabase.sqlConnection);
                //Create a data reader and Execute the command
                MySqlDataReader dataReader = cmd.ExecuteReader();

                //Read the data and store them in the list
                while (dataReader.Read())
                {
                    list.Add(
                        new Cart
                        (
                            dataReader["personid"] + "",
                            dataReader["foodstring"] + "",
                            dataReader["deliverdate"] + "",
                            dataReader["registerdate"] + ""
                        )
                        );
                }

                //close Data Reader
                dataReader.Close();

                //close connection
                myRestaurantDatabase.CloseConnection();

                //return list to be displayed
                return list;
            }
            else
            {
                return list;
            }
        }

        public Cart FindPersonCart(string personID)
        {
            List<Cart> cartsList = this.Select();
            Cart targetCart = cartsList.Where(x => x.personID == personID).First();
            return targetCart;
        }

        public CartFoods ReadCartData(string personID)
        {
            Cart personCart = this.FindPersonCart(personID);
            if(personCart.foodString != "" && personCart.foodString != null)
            {
                FoodDataManager foodDataManager = new FoodDataManager();
                string[] eachFood = personCart.foodString.Split(',');
                Dictionary<Food, int> dict = new Dictionary<Food, int>();
                List<Food> tempFoodList = foodDataManager.Select();
                for(int i = 0; i < eachFood.Length; i++)
                {
                    string[] foodCodeAmount = eachFood[i].Split('-');
                    if(tempFoodList.Where(x => x.code == foodCodeAmount[0]).Any())
                        dict.Add(tempFoodList.Where(x => x.code == foodCodeAmount[0]).First(), int.Parse(foodCodeAmount[1]));
                }

                return new CartFoods(personID, dict);
            }
            return null;
        }
    }

    class CartFoods
    {
        public string personID { get; }
        public Dictionary<Food, int> cartFoodData { get; } = null;
        public CartFoods(string personID, Dictionary<Food, int> cartFoodData)
        {
            this.personID = personID;
            this.cartFoodData = cartFoodData;
        }
    }

    public class CartGrid
    {
        public string code { get; }
        public string name { get; }
        public int amount { get; }
        public float pricePerUnit { get; }
        public CartGrid(string code, string name, int amount, float pricePerUnit)
        {
            this.code = code;
            this.name = name;
            this.amount = amount;
            this.pricePerUnit = pricePerUnit;
        }
    }

}
