﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FoodOrderingSystem
{
    /// <summary>
    /// Interaction logic for AdminCalendar.xaml
    /// </summary>
    public partial class AdminCalendar : Window
    {
        Food selectedFood;
        public AdminCalendar(Food selectedFood)
        {
            InitializeComponent();
            this.selectedFood = selectedFood;
        }

        private void ApplyButton_Click(object sender, RoutedEventArgs e)
        {
            string date = DateTextBox.Text;
            string foodcode = selectedFood.code;
            int amount;
            try
            {
                CalendarDataManager calendarDataManager = new CalendarDataManager();
                if (calendarDataManager.readDayData(date).Count == 7)
                    throw new Exception("Can't add food to selected date. Maximum limit of 7 reached.");

                amount = int.Parse(AmountTextBox.Text);
                if (amount <= 0)
                    throw new ArgumentException("0 or less is not accepted.");

                List<DayFood> dayFoodList = calendarDataManager.Select();
                string tableName = TableName.Calendar.ToString().ToLower();

                // if this date doesn't exist, add both the day and the food with amount
                if (!dayFoodList.Where(x => x.dayOfWeek == date).Any())
                {
                    Dictionary<string, string> dict = new Dictionary<string, string>()
                    {
                        ["datestring"] = $"'{date}'",
                        ["dayfoodstring"] = $"'{foodcode}-{amount},'"
                    };
                    App.db.Insert(dict, tableName);
                }
                // if the date already exists
                else
                {
                    calendarDataManager.UpdateCalendar(date, selectedFood, amount);
                }

                this.Close();

            }
            catch(Exception ex)
            {
                AmountTextBox.Text = "";
                MessageBox.Show(ex.Message, "AddToDayTimeError!");
            }
            
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.FoodTextBlock.Text = selectedFood.name;
            ShowDate(DateTime.Today);
            this.AdminFoodCalendar.DisplayDateStart = DateTime.Today;
            this.AdminFoodCalendar.DisplayDateEnd = DateTime.Today.AddMonths(1);
        }

        private void AdminFoodCalendar_SelectedDatesChanged(object sender, SelectionChangedEventArgs e)
        {
            ShowDate((DateTime)AdminFoodCalendar.SelectedDate);
        }

        private void ShowDate(DateTime date) => DateTextBox.Text = $"{date.Month}/{date.Day}/{date.Year}";

        private void AmountTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (AmountTextBox.Text != "")
                ApplyButton.IsEnabled = true;
            else
                ApplyButton.IsEnabled = false;
        }
    }
}
